% !TEX root = ../resPi_JLAMP.tex
\section{Session-based \pic}
\label{sec:host_calculus}

In this section we present the syntax and semantics definitions of the host language 
considered for our reversible calculus. This is a variant of \pic\ 
enriched with primitives for managing structured binary sessions.
%\footnote{In particular, 
%as discussed in Section~\ref{sect:relatedWork}, we consider the well-established 
%calculus introduced in \cite{YoshidaV07}, whose notation has been revised according to that used in 
%more recent works on binary session types (in particular, \cite{MostrousY09}).}.
%
%A (binary) session is a series of reciprocal interactions between two parties, possibly with branching and recursion. Interactions on a session are performed via a dedicated private channel, which is generated when initiating the session.
%


\subsection{Syntax}
\label{sec:syntax_pic}
%We use the following base sets: 
%\emph{variables} (ranged over by $x$, $y$, \ldots),
%\emph{shared channels} (ranged over by $a$, $b$, \ldots),
%\emph{session channels} (ranged over by $s$, $s'$, \ldots), 
%\emph{session endpoints} (including session channels, ranged over by $s$, $s'$,\ldots, $\ce{s}$, $\ce{s\,}'$,\ldots),
%\emph{labels} (ranged over by $l$, $l'$, \ldots),
%and  \emph{process variables} (ranged over by $X$, $Y$, \ldots).
%%
%Letters $u$, $u'$, \ldots denote \emph{shared identifiers}, i.e. shared channels and variables together. 
%Letters $k$, $k'$, \ldots denote \emph{session identifiers}, i.e. session endpoints and variables together. 
%Letters $c$, $c'$, \ldots denote \emph{channels}, i.e. session channels and shared channels together. 
%%
%\emph{Values}, including boolean ($\ctrue$, $\cfalse$), integers (0, 1, \ldots) and shared channles
%and session endpoints, are ranged over by $v$, $v'$, \ldots.

We use the following base sets: 
\emph{shared channels}, used to initiate sessions;
\emph{session channels}, consisting on pairs of \emph{endpoints} used by the two parties 
to exchange \emph{values} within an established session;
\emph{variables}, used to store values;
\emph{labels}, used to select and offer branching choices;
and  \emph{process variables}, used for recursion. 
%
The corresponding notation and terminology are as follows:

$$
\hspace*{3.4cm}
\begin{array}{l}
\left.
\begin{array}{l}
\text{Variables:}\ x,y,\ldots\\[.1cm]
\text{Shared channels:}\ a,b,\ldots\\
\end{array}
\right\} \text{Shared identifiers:}\ u,u',\ldots
\vspace*{-.5cm}
\\
\hspace*{-3.56cm}
\text{Channels:}\ c,c',\ldots\left\{
\begin{array}{l}
\\[.09cm]
\text{Session channels}\ s,s'\ldots
\end{array}
\right.
\\[.3cm]
\left.
\begin{array}{l}
\text{Variables:}\ x,y,\ldots\\
\, \text{Session endpoints:}\ s,\ce{s},\ldots\\
\end{array}
\right\} \text{Session identifiers:}\ k,k',\ldots
\\[.4cm]
\hspace*{.3cm} \text{Labels:}\ l, l',\ldots\\
\hspace*{.3cm} \text{Process variables:}\ X, Y,\ldots\\
%
\end{array}
$$
\\[-.9cm]
$$
\begin{array}{l}
%
\hspace*{-4.5cm}
\text{Values:}\ v,v',\ldots \left\{
\begin{array}{l}
\text{Booleans:}\ \ctrue,\cfalse\\
\text{Integers:}\ 0,1,\ldots\\
\text{Shared channels:}\ a,b,\ldots\\
\text{Session endpoints:}\ s,\ce{s},\ldots\\
\end{array}
\right.
\end{array}
$$
Notably, each session channel $s$ has two (dual) endpoints, denoted by $s$ and $\ce{s}$, 
each one assigned to one session party to exchange values with the other.  
We define duality to be idempotent, i.e. $\ce{\ce{s}}=s$.
The use of two separated endpoints is similar to that of \emph{polarities} in \cite{GayH05,YoshidaV07}. 
%
Notation $\tuple{\cdot}$ stands for tuples, e.g. $\tuple{c}$ means $c_1,\ldots,c_n$.

\emph{Processes}, ranged over by $P$, $Q$, \ldots, and 
\emph{expressions}, ranged over by $e$, $e'$, \ldots are given by the grammar in Figure~\ref{fig:syntax_pi}. 
We use $\text{op}(\cdot,\ldots,\cdot)$ to denote a generic expression operator; we assume that expressions 
are equipped with standard operators on boolean and integer values (e.g., $\wedge$, $+$, \ldots).
\begin{figure}[t]
\small
	\begin{centering}
	\begin{tabular}{rcl@{\hspace{-.1cm}}r}
	\textbf{Processes}\quad
	$P$ & ::= & $\requestAct{u}{x}{P}$ & connect\\
	& $\mid$ & $\acceptAct{u}{x}{P}$ & connect dual\\
	& $\mid$ & $\sendAct{k}{e}{P}$ & output\\
	& $\mid$ & $\receiveAct{k}{x}{P}$ & input\\
	& $\mid$ & $\selectAct{k}{l}{P}$ & selection\\
	& $\mid$ & $\branchAct{k}{\branch{l_1}{P_1} \branchSep \ldots \branchSep \branch{l_n}{P_n}}$ & branching\\
	& $\mid$ & $\ifthenelseAct{e}{P}{Q}$ & conditional choice\\
	& $\mid$ & $P \mid Q$ & parallel composition\\
	& $\mid$ & $\res{c}\, P$ & restriction\\		
	& $\mid$ & $X$ & process variable\\
	& $\mid$ & $\recAct{X}{P}$ & recursion\\		
	& $\mid$ & $\inact$ & inaction\\	
	\\
	\textbf{Expressions}\quad
	%$e$ & ::= & $v \ \ \mid\ \ x \ \ \mid\ \ \text{op}(e_1,\ldots,e_n)$  & expressions
	$e$ & ::= & $v$ & value\\	
	& $\mid$ & $x$ & variable\\		
	& $\mid$ & $\text{op}(e_1,\ldots,e_n)$ & composed expression
	\\[.2cm]
	\hline
	\end{tabular}
	\vspace*{-.2cm}
	\caption{Session-based \pic: syntax}
	\label{fig:syntax_pi}
	\end{centering}
\end{figure}

The initiation of a session is triggered by the synchronisation on a shared channel $a$ 
of two processes of the form  $\requestAct{a}{x}{P}$ and 
$\acceptAct{a}{y}{Q}$.
This causes the generation of a fresh session channel $s$, 
whose endpoints replace variables $x$ and $y$, by means of a substitution application, 
in order to be used by $P$ and $Q$, respectively, for later communications. 
%
Primitives $\sendAct{k}{e}{P}$ and $\receiveAct{k'}{x}{Q}$ 
denote output and input via session endpoints identified by $k$ and $k'$, respectively. 
These communication primitives realise the standard synchronous message passing, where messages result 
from the evaluation of expressions. Notably, an exchanged value can be 
an endpoint that is being used in a session (this channel-passing modality is 
called \emph{delegation}), thus allowing complex nested structured communications. 
%
Constructs $\selectAct{k}{l}{P}$ and 
$\branchAct{k'}{\branch{l_1}{P_1}\branchSep\ldots\branchSep \branch{l_n}{P_n}}$
denote label selection and label branching (where $l_1$, \ldots, $l_n$ are assumed to 
be pairwise distinct) via endpoints identified by $k$ and $k'$, respectively. 
They mime method invocation in object-based programming. 
%

The above interaction primitives are then combined by standard process calculi 
constructs: 
conditional choice, parallel composition, restriction, 
recursion 
%\footnote{The language presented so far slightly differs from \cite{YoshidaV07}, 
%because we used recursion instead of process definitions. Indeed, recursion is easier to deal 
%within a theoretical framework because the syntax of a recursive term already contains all the 
%code needed to properly run the term itself.}
and the empty process (denoting inaction).
%
It is worth noticing that restriction can have both shared and session channels as argument:
$\res{a}P$ states that $a$ is a private shared channel of $P$; similarly, $\res{s}P$ states that 
the two endpoints of the session channel, namely $s$ and $\ce{s}$, are invisible from processes 
different from $P$ (see the seventh law in Figure~\ref{fig:congruence_pic}), 
i.e. no external process can perform a session action on either of these 
endpoints (this ensures non-interference within a session).
%
As a matter of notation, we will write $\res{c_1,\ldots,c_n}P$
in place of  $\res{c_1}\ldots\res{c_n}P$.

We adopt the following conventions about the operators precedence: 
prefixing, restriction, and recursion bind more tightly than parallel composition.

Bindings are defined as follows:
%
$\requestAct{u}{x}{P}$, $\acceptAct{u}{x}{P}$ and $\receiveAct{k}{x}{P}$
bind variable $x$ in $P$;
$\res{a}\, P$ binds shared channel $a$ in $P$;
%
$\res{s}\, P$ binds session channel $s$ in $P$;
%
finally, $\recAct{X}{P}$ binds process variable $X$ in $P$.
%
The derived notions of bound and free names, alpha-equivalence $\alphaeq$, and 
substitution are standard. 
For $P$ a process, 
$\freev{P}$ denotes the set of \emph{free variables}, 
$\freec{P}$ denotes the set of \emph{free shared channels},
and $\freese{P}$ the set of \emph{free session endpoints}. 
%We also define 
%$\freeu{P} = \freen{P} \cup \freec{P}$.
%
For the sake of simplicity, we assume that free and bound variables are always chosen 
to be different, and that bound variables are pairwise distinct; the same applies to names.
Of course, these conditions are not restrictive and can always be fulfilled by possibly 
using alpha-conversion.

\subsection{Semantics}
The operational semantics is given in terms of a structural congruence and of a reduction relation. 
Notably, the semantics is only defined for \emph{closed} terms, i.e. terms without free variables. 
Indeed, we consider the binding of a variable as its declaration (and initialisation), 
therefore free occurrences of variables at the outset in a term must be prevented 
since they are similar to uses of variables before their declaration in programs 
(which are considered as programming errors).

The \emph{structural congruence}, written $\congr$, is defined as the smallest congruence relation 
on processes that includes the equational laws shown in Figure~\ref{fig:congruence_pic}. 
These are the standard laws of \pic. 
Reading the laws in Figure~\ref{fig:congruence_pic} by row from left to right, and from top to bottom row,
the first three are the monoid laws for $\mid$ (i.e., it is associative and commutative, and has 
$\inact$ as identity element).
The second four laws deal with restriction and enable 
garbage-collection of channels, scope extension and scope swap, respectively.
The eighth law permits unfolding a recursion (notation $P\subst{Q}{X}$ denotes 
replacement of free occurrences of $X$ in $P$ by process $Q$).
The last law equates alpha-equivalent processes, i.e.~processes only differing in the identity of 
bound variables/channels.

%\begin{figure}[t]
%	\begin{centering}	
%	\begin{tabular}{l@{\qquad\ \ }l@{\qquad\ \ }l}	
%	$(P \mid Q) \mid R \congr P \mid (Q \mid R)$ 
%	&
%	$P \mid Q \congr Q \mid P$ 
%	&
%	$P \mid \inact \congr P$
%	\\[.3cm]
%	$\res{a}P \mid Q \congr \res{a}(P \mid Q)$\ \ if $a \notin \freec{Q}$
%	&
%	$\res{c_1}\res{c_2}P \congr \res{c_2}\res{c_1}P$
%	&
%	$\res{c} \inact \congr \inact$
%	\\[.3cm]
%	$\res{s}P \mid Q \congr \res{s}(P \mid Q)$\ \ if $s,\ce{s} \notin \freese{Q}$
%	&
%	$\recAct{X}{P} \congr P\subst{\recAct{X}{P}}{X}$
%	&
%	$P \congr Q$\ \ if $P \alphaeq Q$
%	\\[.2cm]
%	\hline
%	\end{tabular}
%	\vspace*{-.2cm}
%	\caption{Session-based \pic: structural congruence}
%	\label{fig:congruence_pic}
%	%\vspace*{.4cm}
%	\end{centering}
%\end{figure}
\begin{figure}[t]
\small
	\begin{centering}	
	\begin{tabular}{l@{\qquad\ \ }l}	
	$(P \mid Q) \mid R \congr P \mid (Q \mid R)$ 
	&
	$P \mid Q \congr Q \mid P$ 
	\\[.3cm]
	$P \mid \inact \congr P$
	&
	$\res{c} \inact \congr \inact$
	\\[.3cm]
	$\res{a}P \mid Q \congr \res{a}(P \mid Q)$\ \ if $a \notin \freec{Q}$
	&
	$\res{c_1}\res{c_2}P \congr \res{c_2}\res{c_1}P$
	\\[.3cm]
	$\res{s}P \mid Q \congr \res{s}(P \mid Q)$\ \ if $s,\ce{s} \notin \freese{Q}$
	&
	$\recAct{X}{P} \congr P\subst{\recAct{X}{P}}{X}$
	\\[.3cm]
	$P \congr Q$\ \ if $P \alphaeq Q$
	\\[.2cm]
	\hline
	\end{tabular}
	\vspace*{-.2cm}
	\caption{Session-based \pic: structural congruence}
	\label{fig:congruence_pic}
	\end{centering}
\end{figure}


To define the reduction relation, we use an auxiliary function $\expreval{\cdot}{\,}$ 
for evaluating closed expressions: 
$\expreval{e}{v}$ says that expression $e$ evaluates to value $v$ 
(where $\expreval{v}{v}$, and $\expreval{x}{\,}$ is undefined).

The \emph{reduction relation}, written $\red$, is the smallest relation on 
closed processes generated by the rules in Figure~\ref{fig:reduction_pic}.
%\begin{figure}[t]
%	\begin{centering}
%	\begin{tabular}{r@{\ \ }l@{\ \ }l@{\quad}l@{\quad}l}
%	$\requestAct{a}{x}{P_1}\ \mid \ \acceptAct{a}{y}{P_2}$
%	& $\red$ &
%	$\res{s}(P_1\subst{\ce{s}}{x} \mid P_2\subst{s}{y})$ 
%	& $s,\ce{s} \notin \freese{P_1,P_2}$ & \rulelabel{Con}
%	\\[.3cm]
%	$\sendAct{\ce{k}}{e}{P_1} \ \mid \ \receiveAct{k}{x}{P_2}$
%	& $\red$ &
%	$P_1 \mid P_2\subst{v}{x}$
%	& $(k=s\ \, \text{or}\ \, k=\ce{s}),\ \expreval{e}{v}$ & \rulelabel{Com}
%	\\[.3cm]
%	$\selectAct{\ce{k}}{l_i}{P} \ \mid \ \branchAct{k}{\branch{l_1}{P_1}  \branchSep \ldots \branchSep \branch{l_n}{P_n}}$
%	& $\red$ &
%	$P \mid P_i$
%	& $(k=s\ \, \text{or}\ \, k=\ce{s}),\ 1 \leq i \leq n$ & \rulelabel{Lab}
%	\\[.3cm]
%	$\ifthenelseAct{e}{P_1}{P_2}$
%	& $\red$ &
%	$P_1$
%	& $\expreval{e}{\ctrue}$ & \rulelabel{If1}
%	\\[.3cm]
%	$\ifthenelseAct{e}{P_1}{P_2}$
%	& $\red$ &
%	$P_2$
%	& $\expreval{e}{\cfalse}$ & \rulelabel{If2}
%	\\[.3cm]
%%	$P \ \red\ P' \quad \imply\quad P \mid \ Q$
%%	& $\red$ &
%%	$P' \mid Q$
%%	& & \rulelabel{Par}
%%	\\[.3cm]
%%	$P \ \red\ P' \quad \imply\quad \res{c}P$
%%	& $\red$ &
%%	$\res{c}P'$
%%	& & \rulelabel{Res}
%%	\\[.3cm]
%%	$P \congr P'\ $ and $\ P' \, \red\, Q'\ $ and $\ Q \congr Q' \quad \imply \quad P$
%%	& $\red$ &
%%	$Q$
%%	& & \rulelabel{Str}
%	\end{tabular}
%	\begin{tabular}{c}
%	$
%	\infer[$\ \rulelabel{Par}$]{P \mid \ Q \ \red\ P' \mid Q}
%	{P \ \red\ P'}
%	$	
%	\qquad\qquad
%	$
%	\infer[$\ \rulelabel{Res}$]{\res{c}P \ \red\ \res{c}P'}
%	{P \ \red\ P'}
%	$
%	\qquad\qquad
%	$
%	\infer[$\ \rulelabel{Str}$]{P \ \red\ Q}
%	{P \congr P'\ \red\ Q' \congr Q}
%	$
%	\\[.2cm]
%	\hline
%	\end{tabular}
%	\vspace*{-.2cm}
%	\caption{Session-based \pic: reduction relation}
%	\label{fig:reduction_pic}
%	%\vspace*{-.4cm}
%	\end{centering}
%\end{figure}
\begin{figure}[t]
\small
	\begin{centering}
	\begin{tabular}{@{}l@{\quad}l@{\,}l@{}}
	$\requestAct{a}{x}{P_1}\ \mid \ \acceptAct{a}{y}{P_2}$
	$\ \red\ $
	$\res{s}(P_1\subst{\ce{s}}{x} \mid P_2\subst{s}{y})$ 
	& $s,\ce{s} \notin \freese{P_1,P_2}$ & \rulelabel{Con}
	\\[.3cm]
	$\sendAct{\ce{k}}{e}{P_1} \ \mid \ \receiveAct{k}{x}{P_2}$
	$\ \red\ $
	$P_1 \mid P_2\subst{v}{x}$
	& $(k=s\ \, \text{or}\ \, k=\ce{s}),\ \expreval{e}{v}$ & \rulelabel{Com}
	\\[.3cm]
	$\selectAct{\ce{k}}{l_i}{P} \ \mid \ \branchAct{k}{\branch{l_1}{P_1}  \branchSep \ldots \branchSep \branch{l_n}{P_n}}$
	$\ \red\ $
	$P \mid P_i$
	& $(k=s\ \, \text{or}\ \, k=\ce{s}),\ 1 \leq i \leq n$ & \rulelabel{Lab}
	\\[.3cm]
	$\ifthenelseAct{e}{P_1}{P_2}$
	$\ \red\ $
	$P_1$
	& $\expreval{e}{\ctrue}$ & \rulelabel{If1}
	\\[.3cm]
	$\ifthenelseAct{e}{P_1}{P_2}$
	$\ \red\ $
	$P_2$
	& $\expreval{e}{\cfalse}$ & \rulelabel{If2}
	\\[.3cm]
	\end{tabular}
	\begin{tabular}{@{}c@{}}
	$
	\infer[$\rulelabel{Par}$]{P \mid \ Q \ \red\ P' \mid Q}
	{P \ \red\ P'}
	$	
	\quad\ \
	$
	\infer[$\rulelabel{Res}$]{\res{c}P \ \red\ \res{c}P'}
	{P \ \red\ P'}
	$
	\quad\ \
	$
	\infer[$\rulelabel{Str}$]{P \ \red\ Q}
	{P \congr P'\ \red\ Q' \congr Q}
	$
	\\[.2cm]
	\hline
	\end{tabular}
	\vspace*{-.4cm}
	\caption{Session-based \pic: reduction relation}
	\label{fig:reduction_pic}
	%\vspace*{-.4cm}
	\end{centering}
\end{figure}
We comment on salient points. 
%
A new session is established when two parallel processes synchronise 
via a shared channel $a$; this results on the generation of a fresh (private) session 
channel whose endpoints are assigned to the two session parties (rule \rulelabel{Con}).
%
During a session, the two parties can exchange values (for data- and channel-passing, 
rule \rulelabel{Com}) and labels (for branching selection, rule \rulelabel{Lab}). 
%
The other rules are standard and state that: 
conditional choice evolves according to the evaluation of the expression argument 
(rules \rulelabel{If1} and \rulelabel{If2});
if a part of a larger process evolves, the whole process evolves accordingly 
(rules \rulelabel{Par} and \rulelabel{Res});
and structural congruent processes have the same reductions (rule \rulelabel{Str}). 


\subsection{The multiple providers scenario in the session-based \pic}
\label{sec:example}

%We introduce here a simple scenario that will be used throughout the paper as a running 
%example to illustrate our approach. 
%The considered scenario involves a \emph{client} and multiple \emph{providers} offering 
%the same service (e.g., on-demand video streaming). 
%The client starts by opening a session with a provider (corresponding to the login phase)
%and sends its service request (e.g., title of a movie, video quality, minimum bandwidth, etc.). 
%Then, the provider sends a quote for the request determined according to the requested 
%quality of service and to the servers status (e.g., current load, available bandwidth, etc.). 
%The client can either accept, negotiate or reject the quote. In the first two cases,
%the interaction between the two parties shall continue. 

The scenario involving a client and multiple providers introduced in Section~\ref{sec:intro} 
can be rendered in \mbox{\pic}\ as follows (for the sake of simplicity,
here we consider just two providers):
$$
P_{client}  \ \mid \ P_{provider1}  \ \mid \ P_{provider2}  
$$
where the client process $P_{client}$ is defined as
$$
\begin{array}{l}
\requestPrefix{a_{login}}{x}.\,\send{x}{\mathsf{srv\_req}}.\,\receive{x}{y_{quote}}.\\
\quad
\ifPi\ accept(y_{quote})\ \thenPi\ \selectAct{x}{l_{acc}}{\,P_{acc}}\\
%\hspace*{4.5cm}
\quad
\elsePi\ (\ifPi\ negotiate(y_{quote})\ \thenPi\ \selectAct{x}{l_{neg}}{\,P_{neg}}\ 
\elsePi\ \selectAct{x}{l_{rej}}{\,\inact})
\end{array}
$$
while a provider process $P_{provider\,i}$ is as follows
$$
\begin{array}{l}
\acceptPrefix{a_{login}}{y}.\,\receive{y}{z_{req}}.\,\send{y}{quote_i(z_{req})}.\,
\branchAct{y}{\branch{l_{acc}}{Q_{acc}} \ \branchSep\ \branch{l_{neg}}{Q_{neg}} \ \branchSep\ \branch{l_{rej}}{\inact}}
\end{array}
$$

We show below a possible evolution of the system, where the client contacts $provider1$ and 
accepts the proposed quote: 
$$
\begin{array}{@{}l@{}}
P_{client}  \ \mid \ P_{provider1}  \ \mid \ P_{provider2}  
\\[.1cm]
\red
\\[.1cm]
\res{s}(\,\send{\ce{s}}{\mathsf{srv\_req}}.\,\receive{\ce{s}}{y_{quote}}.\,
\ifPi\ accept(y_{quote})\ \thenPi\ \selectAct{\ce{s}}{l_{acc}}{\,P_{acc}\subst{\ce{s}}{x}}
\elsePi\ (\ldots)
\\
\hspace{.8cm}
\mid\ 
\receive{s}{z_{req}}.\,\send{s}{quote_i(z_{req})}.\,
\branchAct{s}{\branch{l_{acc}}{Q_{acc}\subst{s}{y}} \ \branchSep\ \branch{l_{neg}}{Q_{neg}\subst{s}{y}} \ \branchSep\ \branch{l_{rej}}{\inact}}\,)
\\
\mid \ P_{provider2}  
\\[.1cm]
\red
\\[.1cm]
\res{s}(\,\receive{\ce{s}}{y_{quote}}.\,
\ifPi\ accept(y_{quote})\ \thenPi\ \selectAct{\ce{s}}{l_{acc}}{\,P_{acc}\subst{\ce{s}}{x}}
\elsePi\ (\ldots)
\\
\hspace{.8cm}
\mid\ 
\send{s}{quote_i(\mathsf{srv\_req})}.\,
\branchAct{s}{\branch{l_{acc}}{Q_{acc}\subst{s}{y}\subst{\mathsf{srv\_req}}{z_{req}}} \ \branchSep\  \ldots}\,)
\\
\mid \ P_{provider2}  
\\[.1cm]
\red
\\[.1cm]
\res{s}(\,\ifPi\ accept(\mathsf{quote})\ \thenPi\ \selectAct{\ce{s}}{l_{acc}}{\,P_{acc}\subst{\ce{s}}{x}\subst{\mathsf{quote}}{y_{quote}}}
\elsePi\ (\ldots)
\\
\hspace{.8cm}
\mid\ 
\branchAct{s}{\branch{l_{acc}}{Q_{acc}\subst{s}{y}\subst{\mathsf{srv\_req}}{z_{req}}} \ \branchSep\  \ldots}\,)
\\
\mid \ P_{provider2}  
\\[.1cm]
\red
\\[.1cm]
\res{s}(\,\selectAct{\ce{s}}{l_{acc}}{\,P_{acc}\subst{\ce{s}}{x}\subst{\mathsf{quote}}{y_{quote}}}
\ \mid\ 
\branchAct{s}{\branch{l_{acc}}{Q_{acc}\subst{s}{y}\subst{\mathsf{srv\_req}}{z_{req}}} \ \branchSep\  \ldots}\,)
\\
\mid \ P_{provider2}  
\\[.1cm]
\red
\\[.1cm]
\res{s}(\,P_{acc}\subst{\ce{s}}{x}\subst{\mathsf{quote}}{y_{quote}} \ \mid \
Q_{acc}\subst{s}{y}\subst{\mathsf{srv\_req}}{z_{req}} \,)
\\
\mid \ P_{provider2}  

\end{array}
$$



