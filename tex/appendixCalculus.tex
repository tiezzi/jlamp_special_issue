% !TEX root = ../resPi_JLAMP.tex
\section{Proofs}
\label{app:properties}

\subsection{\emph{Proofs of Section~\ref{sec:properties}}}

\subsubsection{Correspondence with \pic}
\label{app:Correspondence}

\lemmaApp{\ref{lem:str_preserved}}{
Let $M$ and $N$ be two \respi\ processes. If $M \congr N$ then $\forget{M} \congr \forget{N}$.
}
\begin{proof}
We proceed by induction on the derivation of $M \congr N$. For most of the laws 
in Figure~\ref{fig:congruence_respi} the conclusion is trivial because 
$\forget{M} \congr \forget{N}$ directly corresponds to a law for \pic\ in Figure~\ref{fig:congruence_pic}.
Instead, for the eighth and twelve laws, we easily conclude because by applying $\forgetMap$
we obtain the identity. 
\end{proof}

\bigskip

\lemmaApp{\ref{lem:correspondence}}{
Let $M$ and $N$ be two \respi\ processes. If $M \fwred N$ then $\forget{M} \red \forget{N}$.
}
\begin{proof}
We proceed by induction on the derivation of $M \fwred N$. Base cases:
\begin{itemize}
\item \rulelabel{fwCon}: We have that 
$M \ = \ \ptag{t_1}\requestAct{a}{x}{P_1}\ \mid \ \ptag{t_2}\acceptAct{a}{y}{P_2}$
\ \ and \ \ 
$N \ = \ \res{s,t_1',t_2'}(
	\ptag{t_1'}P_1\subst{\ce{s}}{x} \mid \ptag{t_2'}P_2\subst{s}{y}
	\mid \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'})$
with $s,\ce{s} \notin \freese{P_1,P_2}$. By definition of $\forgetMap$, we obtain
$\forget{M} \ = \ \requestAct{a}{x}{P_1}\ \mid \ \acceptAct{a}{y}{P_2}$.
Now, by applying rules \rulelabel{Con} and \rulelabel{Str}, we have
$\forget{M} \red \res{s}(P_1\subst{\ce{s}}{x} \mid P_2\subst{s}{y} \mid \inact)\ =\ P$.
By definition of $\forgetMap$, we have $\forget{N}=P$ that permits to conclude.
\item \rulelabel{fwCom},\rulelabel{fwLab},\rulelabel{fwIf1},\rulelabel{fwIf2}: 
These cases are similar to the previous one.
\end{itemize}
Inductive cases:
\begin{itemize}
\item \rulelabel{fwPar}: We have that 
$M \ = \ M_1 \mid \ M_2$
\ \ and \ \
$N \ = \ M_1' \mid \ M_2$.
By the premise of rule \rulelabel{fwPar}, we also have $M_1 \ \fwred\ M_1'$ from which, 
by induction, we obtain
$\forget{M_1} \ \red\ \forget{M_1'}$.
By definition of $\forgetMap$, we get
$\forget{M}\ = \ \forget{M_1} \mid \ \forget{M_2}$.
By applying rule \rulelabel{Par}, we have
$\forget{M} \red \forget{M_1'} \mid \ \forget{M_2} \ =\ P$.
Thus, by definition of $\forgetMap$, we have
$\forget{N} \ = \ P$ that directly permits concluding.
\item \rulelabel{fwRes}: 
This case is similar to the previous one. In particular, when the restricted name is a tag, 
it is not even necessary to apply rule \rulelabel{Res}, because the forgetful map erases 
the restriction.
\item \rulelabel{fwStr}: 
By the premise of rule \rulelabel{fwStr}, we have 
$M  \congr M'$,
$M' \fwred N'$
and $N'  \congr N$.
By induction, we obtain
$\forget{M'} \red \forget{N'}$. 
By applying Lemma~\ref{lem:str_preserved},
we have $\forget{M}  \congr \forget{M'}$ and $\forget{N'} \congr \forget{N}$
that allow us to conclude.
\end{itemize}
\vspace*{-1.3cm}
\end{proof}

\bigskip


\lemmaApp{\ref{lem:str_preserved_inv}}{
Let $P$ and $Q$ be two \pic\ processes. If $P \congr Q$ then for 
any \respi\ process $M$ such that $\forget{M}=P$ there exists a \respi\ process
$N$ such that $\forget{N}=Q$ and $M \congr N$.
}
\begin{proof}
The proof is straightforward. Indeed, given a \respi\ process $M$ such that $\forget{M}=P$,
it must have the form $\res{\tuple{t}}(\ptag{t}P \mid \prod_{i\in I}m_i)$ up to $\congr$. Thus, 
the process $N$, such that $\forget{N}=Q$, can be defined accordingly: 
$N \congr \res{\tuple{t}}(\ptag{t}Q \mid \prod_{i\in I}m_i)$. Now, we can conclude 
by exploiting the ninth law in Figure~\ref{fig:congruence_respi}, 
i.e. $\ptag{t}P \congr \ptag{t}Q$\ \ if $P \congr Q$, and the fact that relation $\congr$ on
\respi\ processes is a congruence. 
\end{proof}

\bigskip

\lemmaApp{\ref{lem:correspondence_inv}}{
Let $P$ and $Q$ be two \pic\ processes. If $P \red Q$ then 
for any \respi\ process $M$ such that $\forget{M}=P$ there exists a \respi\ process
$N$ such that $\forget{N}=Q$ and $M \fwred N$.
}
\begin{proof}
We proceed by induction on the derivation of $P \red Q$. Base cases:
\begin{itemize}
\item \rulelabel{Con}: We have that 
$P \, = \, \requestAct{a}{x}{P_1} \mid \acceptAct{a}{y}{P_2}$
 \ and\ \
$Q \, = \, \res{s}(P_1\subst{\ce{s}}{x} \mid P_2\subst{s}{y})$ 
with $s,\ce{s} \notin \freese{P_1,P_2}$.
Let $M$ be a \respi\ process such that $\forget{M}=P$, it must have the form 
$\res{\tuple{t}}(\ptag{t_1}\requestAct{a}{x}{P_1} \mid \ptag{t_2}\acceptAct{a}{y}{P_2} \mid \prod_{i\in I}m_i)$ up to $\congr$. Thus, by applying rules \rulelabel{fwCon}, \rulelabel{fwPar}, \rulelabel{fwRes} and \rulelabel{fwStr}, 
we get $M \fwred \res{\tuple{t},s,t_1',t_2'}(
\ptag{t_1'}P_1\subst{\ce{s}}{x} \mid \ptag{t_2'}P_2\subst{s}{y}
\mid \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'} \mid \prod_{i\in I}m_i) \, =\, N$.
We conclude by applying $\forgetMap$ to $N$, since we obtain $\forget{N}=Q$.
\item \rulelabel{Com},\rulelabel{Lab},\rulelabel{If1},\rulelabel{If2}: 
These cases are similar to the previous one.
\end{itemize}
Inductive cases:
\begin{itemize}
\item \rulelabel{Par}: We have that 
$P \ = \ P_1 \mid P_2$
\ \ and \ \
$Q \ = \ P_1' \mid P_2$.
Let $M$ be a \respi\ process such that $\forget{M}=P_1 \mid P_2$. 
We have $M \congr \res{\tuple{t}}(\ptag{t_1}P_1 \mid \ptag{t_2}P_2 \mid  \prod_{i\in I}m_i)
\congr \res{\tuple{t'}}(M_1 \mid \ptag{t_2}P_2 \mid  \prod_{j\in J}m_j)$
with $M_1\congr  \res{\tuple{t''}}(\ptag{t_1}P_1 \mid \prod_{k\in K}m_k)$,
$\tuple{t}=\tuple{t'},\tuple{t''}$ and $J\cup K=I$.
By the premise of rule \rulelabel{Par}, we also have $P_1 \red P_1'$ from which, 
by induction, since $\forget{M_1}=P_1$, 
there exists $M_1'$ such that $\forget{M_1'}=P_1'$ and $M_1 \fwred M_1'$.
Thus, by applying rules \rulelabel{fwPar}, \rulelabel{fwRes} and \rulelabel{fwStr}, 
we get 
$M \fwred  \res{\tuple{t'}}(M_1' \mid \ptag{t_2}P_2 \mid  \prod_{j\in J}m_j)\,=\,N$.
We conclude by applying $\forgetMap$ to $N$, because
$\forget{N}=\forget{M_1'} \mid P_2= P_1' \mid P_2 =Q$.
\item \rulelabel{Res}: 
This case is similar to the previous one.
\item \rulelabel{Str}: We have that 
$P \congr P'$, $Q \congr Q'$ and $P' \red Q'$.
Let $M$ be a process such that $\forget{M}=P$.
By applying Lemma~\ref{lem:str_preserved_inv}, 
there exists $M'$ such that $\forget{M'}=P'$ and $M \congr M'$.
By induction, there is $N'$ such that $\forget{N'}=Q'$ and $M' \fwred N'$.
By applying Lemma~\ref{lem:str_preserved_inv} again, 
there exists $N$ such that $\forget{N}=Q$ and $N \congr N'$.
By applying rule \rulelabel{fwStr}, we conclude $M \fwred N$.
\end{itemize}
\vspace*{-1.2cm}
\end{proof}


\subsubsection{Loop lemma}
\label{app:LoopLemma}

\lemmaApp{\ref{lemma:loop}}{
Let $M$ and $N$ be two reachable \respi\ processes. 
$M \fwred N$ if and only if $N \bwred M$.
}
\begin{proof}
The proof for the \emph{if} part is by induction on the derivation of $M \fwred N$.
Base cases:
\begin{itemize}
\item \rulelabel{fwCon}: We have that 
$M \ = \ \ptag{t_1}\requestAct{a}{x}{P_1}\ \mid \ \ptag{t_2}\acceptAct{a}{y}{P_2}$
\ \ and \ \ 
$N \ = \ \res{s,t_1',t_2'}(
	\ptag{t_1'}P_1\subst{\ce{s}}{x} \mid \ptag{t_2'}P_2\subst{s}{y}
	\mid \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'})$
with $s,\ce{s} \notin \freese{P_1,P_2}$. 
By applying rule \rulelabel{bwCon}, we can directly conclude 
$N \bwred M$.
\item \rulelabel{fwCom},\rulelabel{fwLab},\rulelabel{fwIf1},\rulelabel{fwIf2}: 
These cases are similar to the previous one.
\end{itemize}
Inductive cases:
\begin{itemize}
\item \rulelabel{fwPar}: We have that 
$M  =  N_1 \mid N_2$, $N  = N_1' \mid N_2$ and $N_1 \fwred N_1'$.
By induction $N_1' \bwred N_1$. Thus, we conclude by applying rule \rulelabel{bwPar},
since we get $N  = N_1' \mid N_2 \bwred N_1 \mid N_2 = M$.
%\item \rulelabel{fwRes} and \rulelabel{fwStr}: 
%These cases can be proved by straightforwardly resorting to the induction hypothesis as in the previous case.
\item \rulelabel{fwRes}:
We have that 
$M  =  \res{h}M_1$, $N  = \res{h}M_1'$ and $M_1 \fwred M_1'$.
By induction $M_1' \bwred M_1$. Thus, we conclude by applying rule \rulelabel{bwRes},
since we get $N  = \res{h}M_1'  \bwred \res{h}M_1 = M$. 

\item \rulelabel{fwStr}: 
We have that 
$M \congr M'$, $N \congr N'$ and $M' \fwred N'$.
By induction $N' \bwred M'$. Thus, we conclude by applying rule \rulelabel{bwStr},
since we directly get $N  \bwred  M$.
\end{itemize}

\noindent
The proof for the \emph{only if} part is by induction on the derivation of $N \bwred M$.
Base cases:
\begin{itemize}
\item \rulelabel{bwCon}: We have that 
$N \ = \ \res{s,t_1',t_2'}(\ptag{t_1'}P \mid \ptag{t_2'}Q 
	\mid
	 \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'})$
\ \ and \ \ 
$M \ = \ \ptag{t_1}\requestAct{a}{x}{P_1}\ \mid \ \ptag{t_2}\acceptAct{a}{y}{P_2}$.
Since $N$ is a reachable process, memory $\amem{\actEv{t_1}{\,\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'}$
has been generated by a synchronisation between threads
$\ptag{t_1}\requestAct{a}{x}{P_1}$ and $\ptag{t_2}\acceptAct{a}{y}{P_2}$,
producing a session channel $s$ and two continuation processes 
$P_1\subst{\ce{s}}{x}$ and $P_2\subst{s}{y}$ tagged by $t_1'$ and $t_2'$, respectively.
Now, by tag uniqueness implied by reachability and use of restriction in tag generation,
$P$ and $Q$ must coincide with $P_1\subst{\ce{s}}{x}$ and $P_2\subst{s}{y}$, respectively.
Therefore, by applying rule \rulelabel{fwCon}, we can directly conclude $M \fwred N$.
\item \rulelabel{bwCom},\rulelabel{bwLab},\rulelabel{bwIf}: 
These cases are similar to the previous one.
\end{itemize}
Inductive cases:
\begin{itemize}
\item \rulelabel{bwPar}: We have that 
$N  =  N_1 \mid N_2$, $M  = N_1' \mid N_2$ and $N_1 \bwred N_1'$.
By induction $N_1' \fwred N_1$. Thus, we conclude by applying rule \rulelabel{fwPar},
since we get $M  = N_1' \mid N_2 \fwred N_1 \mid N_2 = N$.
%\item \rulelabel{bwRes} and \rulelabel{bwStr}: 
%These cases can be proved by straightforwardly resorting to the induction hypothesis as in the previous case.
\item \rulelabel{bwRes}:
We have that 
$N  =  \res{h}N_1$, $M  = \res{h}N_1'$ and $N_1 \bwred N_1'$.
By induction $N_1' \fwred N_1$. Thus, we conclude by applying rule \rulelabel{fwRes},
since we get $M  = \res{h}N_1'  \fwred \res{h}N_1 = N$. 

\item \rulelabel{bwStr}: 
We have that 
$N \congr N'$, $M \congr M'$ and $N' \bwred M'$.
By induction $M' \fwred N'$. Thus, we conclude by applying rule \rulelabel{fwStr},
since we directly get $M  \fwred  N$.
\end{itemize}
\vspace*{-1.3cm}
\end{proof}



\subsubsection{Causal consistency}
\label{app:causality}

\lemmaApp{\ref{lemma:square}}{
If $\tName_1=M\transition{\tlab_1}M_1$ and $\tName_2=M\transition{\tlab_2}M_2$
are two coinitial concurrent transitions, then there exist two cofinal transitions
$\tName_2/\tName_1=M_1\transition{\tlab_2}N$ and $\tName_1/\tName_2=M_2\transition{\tlab_1}N$.
}
\begin{proof}
By case analysis on the form of transitions $\tName_1$ and $\tName_2$.
\begin{itemize}
\item $M\transition{m_1,\mSet_1,\fwred}M_1$ and $M\transition{m_2,\mSet_2,\fwred}M_2$.
The two transitions can be any combination of forward reductions. Let us consider the case of 
two communication (the other cases are similar). Since the two transitions are concurrent, the involved 
threads  are four distinct threads (two sending threads and two receiving ones). In particular, we consider 
below two communications along different sessions; in fact, the type discipline in Section~\ref{sec:types}
forbids concurrent communications along the same session (although, in this proof, this kind 
of concurrent communications would not cause any problem). Thus, in the considered case, the source 
of the two transitions is as follows:
$$
\begin{array}{rc@{\,}l}
M & \congr  \res{s_1,s_2,\tuple{t}} (& \ptag{t_1}\sendAct{\ce{s_1}}{e_1}{P_1} \ \mid \ \ptag{t_2}\receiveAct{s_1}{x_1}{P_2}
\\
&& \mid \ \ptag{t_3}\sendAct{\ce{s_2}}{e_2}{P_3} \ \mid \ \ptag{t_4}\receiveAct{s_2}{x_2}{P_4} \ \mid \ M'\,)
\end{array}
$$
where $t_1$, $t_2$, $t_3$, $t_4$ are in $\tuple{t}$. 
Then, $M \fwred M_1$ with
$$
\begin{array}{rc@{\,}l}
M_1 & \congr  \res{s_1,s_2,\tuple{t},t_1',t_2'} (& \ptag{t_1'}P_1 \mid \ptag{t_2'}P_2\subst{v_1}{x_1} \mid \ m_1
\\
&& \mid \ \ptag{t_3}\sendAct{\ce{s_2}}{e_2}{P_3} \ \mid \ \ptag{t_4}\receiveAct{s_2}{x_2}{P_4} \ \mid \ M'\,)
\end{array}
$$
where $\expreval{e_1}{v_1}$ and $m_1 = \amem{\actEv{t_1}{\comAct{\ce{s_1}}{e_1}{x_1}{P_1}{P_2}}{t_2}}{t_1'}{t_2'}$.
Similarly, $M \fwred M_2$ with
$$
\begin{array}{rc@{\,}l}
M_2 & \congr  \res{s_1,s_2,\tuple{t},t_3',t_4'} (& \ptag{t_1}\sendAct{\ce{s_1}}{e_1}{P_1} \ \mid \ \ptag{t_2}\receiveAct{s_1}{x_1}{P_2}
\\
&& \mid \ \ptag{t_3'}P_3 \mid \ptag{t_4'}P_4\subst{v_2}{x_2} \mid \ m_2 \ \mid \ M'\,)
\end{array}
$$
where $\expreval{e_2}{v_2}$ and $m_2 = \amem{\actEv{t_3}{\comAct{\ce{s_2}}{e_2}{x_2}{P_3}{P_4}}{t_4}}{t_3'}{t_4'}$.
Now, we have that $M_1 \fwred N$ with
$$
\begin{array}{rc@{\,}l}
N & \congr  \res{s_1,s_2,\tuple{t},t_1',t_2',t_3',t_4'} (& \ptag{t_1'}P_1 \mid \ptag{t_2'}P_2\subst{v_1}{x_1} \mid \ m_1
\\
&& \mid \ \ptag{t_3'}P_3 \mid \ptag{t_4'}P_4\subst{v_2}{x_2} \mid \ m_2 \ \mid \ M'\,)
\end{array}
$$
As desired, we also have that $M_2 \fwred N$.

\item $M\transition{m_1,\mSet_1,\fwred}M_1$ and $M\transition{m_2,\mSet_2,\bwred}M_2$.
The two transitions can be any combination of a forward rule and a backward one, respectively. 
Let us consider the case of a forward communication and the undo of a choice (again, the other cases 
are similar). Thus, in the considered case, the source of the two transitions is as follows:
$$
\begin{array}{rc@{\,}l}
M & \congr  \res{s,\tuple{t},t_3'} (& \ptag{t_1}\sendAct{\ce{s}}{e_1}{P_1} \ \mid \ \ptag{t_2}\receiveAct{s}{x_1}{P_2}
\\
&& \mid \ \ptag{t_3'} P_3\ \mid \ m_2 \ \mid \ M'\,)
\end{array}
$$
where $m_2 = \cmem{t_3}{\ifEv{e_2}{P_3}{P_4}}{t_3'}$, $\expreval{e}{\ctrue}$, 
and  $t_1$, $t_2$, $t_3$ are in $\tuple{t}$. 
Notably, since the two transitions are concurrent, the continuation tag in $m_2$ can be 
neither $t_1$ nor $t_2$ (indeed, in the above process $M$ this tag is $t_3'$).
Then, $M \fwred M_1$ with
$$
\begin{array}{rc@{\,}l}
M_1 & \congr  \res{s,\tuple{t},t_3',t_1',t_2'} (& \ptag{t_1'}P_1 \mid \ptag{t_2'}P_2\subst{v_1}{x_1} \mid \ m_1
\\
&& \mid \ \ptag{t_3'} P_3\ \mid \ m_2 \ \mid \ M'\,)
\end{array}
$$
where $\expreval{e_1}{v_1}$ and $m_1 = \amem{\actEv{t_1}{\comAct{\ce{s}}{e_1}{x_1}{P_1}{P_2}}{t_2}}{t_1'}{t_2'}$.
Now, we also have that $M \bwred M_2$ with
$$
\begin{array}{rc@{\,}l}
M_2 & \congr  \res{s,\tuple{t}} (& \ptag{t_1}\sendAct{\ce{s}}{e_1}{P_1} \ \mid \ \ptag{t_2}\receiveAct{s}{x_1}{P_2}
\\
&& \mid \ \ptag{t_3} \ifthenelseAct{e_2}{P_3}{P_4}  \ \mid \ M'\,)
\end{array}
$$
As desired, both $M_1$ and $M_2$ can then evolve with a backward and forward reduction, respectively, 
to $N$:
$$
\begin{array}{rc@{\,}l}
N & \congr  \res{s,\tuple{t},t_1',t_2'} (& \ptag{t_1'}P_1 \mid \ptag{t_2'}P_2\subst{v_1}{x_1} \mid \ m_1
\\
&& \mid \ \ptag{t_3} \ifthenelseAct{e_2}{P_3}{P_4}  \ \mid \ M'\,)
\end{array}
$$

\item $M\transition{m_1,\mSet_1,\bwred}M_1$ and $M\transition{m_2,\mSet_2,\bwred}M_2$.
Similar to the first case.

\item $M\transition{m_1,\mSet_1,\bwred}M_1$ and $M\transition{m_2,\mSet_2,\fwred}M_2$.
Similar to the second case.
\end{itemize}
\vspace*{-.9cm}
\end{proof}

The proof of the Causal Consistency theorem follows the (standard) pattern used in 
\cite{LaneseMS10,DanosK04}. In particular, the proof relies on two auxiliary lemmas.  
The first lemma permits rearranging a trace as a composition of a backward trace and a 
forward one. The second lemma permits shortening a forward trace. 

\begin{lemma}\label{lemma:rearraging}
Let $\trace$ be a trace. There exist $\trace'$ and $\trace''$ both forward traces such that 
$\trace \causallyEq \inver{\trace'};\trace''$.
\end{lemma}
\begin{proof}
We prove this by lexicographic induction on the length of $\trace$, and the distance to the 
beginning of $\trace$ of the earliest pair of transitions in $\trace$ contradicting the property.
If there is no such contradicting pair, then we are done. 
%
If there is one, say a pair of the form $\tName;\inver{\tName'}$ with $\tName$ and $\tName'$ 
forward transitions, we have two possibilities: either $\tName$ and $\tName'$ are concurrent, 
or they are in conflict. 
%
In the first case, $\tName$ and $\inver{\tName'}$ can be swapped 
by using Lemma~\ref{lemma:square}, resulting in a later earliest contradicting pair. Then, the result follows 
by induction, since swapping transitions keeps the total length constant.
%
In the second case, there is a conflict on a tag, because it belongs to the stamps of 
both transitions. Again, we have two sub-cases: either the memory involved in the two 
transitions is the same or not. In the first sub-case we have $\tName=\tName'$, and then we can 
apply Lemma~\ref{lemma:loop} to remove $\tName;\inver{\tName}$. 
Hence, the total length of $\trace$ decreases and, again, by induction we obtain the thesis. 
Instead, the second sub-case never happens. Indeed, let $\tName$ generate a memory 
$m_1 = \cmem{t}{\ifEv{e}{P}{Q}}{t'}$ (the case with the action memory is similar). A conflict with 
$\tName'$ would be caused by the presence of $t$ or $t'$ in the memory $m_2$ removed by 
$\inver{\tName'}$ (and, by hypothesis, different from $m_1$). However, $t$ cannot occur in $m_2$,
because the transition $\tName$ consumed the thread uniquely tagged by $t$, which then cannot be 
involved in the other transition. Also $t'$ cannot occur in $m_2$, because the thread uniquely tagged 
by $t'$ has been generated by $\tName$; thus, another forward transition must take place before 
$\inver{\tName'}$ to involve this thread so that $t'$ could occur in $m_2$. 
\end{proof}

\begin{lemma}\label{lemma:shortening}
Let $\trace_1$ and $\trace_2$ be coinitial and cofinal traces, with $\trace_2$ forward. 
Then, there exists a forward trace $\trace_1'$ of length at most that of $\trace_1$ such that 
$\trace_1' \causallyEq \trace_1$.
\end{lemma}
\begin{proof}
The proof is by induction on the length of $\trace_1$.   
%
If $\trace_1$ is a forward trace we are already done.
Otherwise, by Lemma~\ref{lemma:rearraging} we can write $\trace_1$ 
as $\inver{\trace};\trace'$ (with $\trace$ and $\trace'$ forward). 
Due to its form, $\trace_1$ contains only one sequence of transitions with opposite direction,
say $\inver{\tName};\tName'$. Let  $m_1$ the memory removed by $\inver{\tName}$. Then, in $\trace'$
there is a forward transition generating $m_1$; otherwise there would be a difference with respect to 
$\trace_2$, since the latter is a forward trace. Let $\tName_1$ be the earliest such transition in $\trace_1$.
Since $\tName_1$ is able to put back $m_1$, it has to be the opposite of $\inver{\tName}$, i.e. 
$\tName_1 = \tName$. 
Now, we can swap $\tName_1$ with all the transitions between $\tName_1$ and $\inver{\tName}$, 
in order to obtain a trace in which $\tName_1$ and $\inver{\tName}$ are adjacent. 
To do so, we use Lemma~\ref{lemma:square}, since all the transitions in between are concurrent.
%
Assume in fact that there is a transition involving memory $m_2$ which is not concurrent to $\tName_1$.
A possible conflict could be caused by the presence of a continuation tag, say $t$, of $m_1$ in $m_2$. 
But this case can never happen, since $t$ is freshly generated by the forward rule used to produce 
$\tName_1$ and thus, thanks to tag uniqueness, $t$ cannot coincide with any tag of a previously 
executed transition. The other possible conflict could be caused by the presence of a continuation tag of $m_2$ 
in $m_1$. Since $\inver{\tName}$ removes $m_1$, this memory cannot contain a fresh tag generated by 
a subsequent transition when $m_2$ is created. Thus, also this case can never happen. 
%
Now, when $\inver{\tName}$ and ${\tName}$ are adjacent, we can remove both of them using 
$\causallyEq$. The resulting trace is shorter, thus the thesis follows by inductive hypothesis. 
\end{proof}

\theoremApp{\ref{th:causalConsistency}}{
Let $\trace_1$ and $\trace_2$ be coinitial traces. Then, $\trace_1 \causallyEq \trace_2$
if and only if $\trace_1$ and $\trace_2$ are cofinal. 
}
\begin{proof} %\emph{(Sketch)}\ \
By construction of $\causallyEq$, if $\trace_1 \causallyEq \trace_2$ then 
$\trace_1$ and $\trace_2$ must be coinitial and cofinal, so this direction of the theorem 
is verified. 
%
%The proof that $\trace_1$ and $\trace_2$ being coinitial and cofinal implies that 
%$\trace_1 \causallyEq \trace_2$ is based on a lemma (Rearranging Lemma),
%which permits expressing the two traces as composition of a backward trace 
%and a forward one, and proceed by lexicographic induction on the sum of the 
%lengths of $\trace_1$ and $\trace_2$ and on the distance between the end of 
%$\trace_1$ and the earliest pair of transitions, one  in $\trace_1$ and another in $\trace_2$, 
%that are not equal. 
%
Thus, it just remains to prove that $\trace_1$ and $\trace_2$ being coinitial and cofinal implies that
$\trace_1 \causallyEq \trace_2$.
By Lemma~\ref{lemma:rearraging}, we know that the two traces can be written as composition of 
a backward trace and a forward one. The proof is by lexicographic induction on the sum of the lengths 
of $\trace_1$ and $\trace_2$ and on the distance between the end of $\trace_1$ and the earliest pair 
of transitions $\tName_1$ in $\trace_1$ and $\tName_2$ in $\trace_2$ which are not equal. 
If all the transitions are equal then we are done. 
Otherwise, we have to consider four cases, depending on whether the two 
transitions are forward or backward.
\begin{itemize}
\item \emph{$\tName_1$ forward and $\tName_2$ backward.}
One has $\trace_1=\inver{\trace};\tName_1;\trace'$ and $\trace_2=\inver{\trace};\tName_2;\trace''$ 
for some $\trace$, $\trace'$, and $\trace''$. 
%
Lemma~\ref{lemma:shortening} applies to $\tName_1;\trace'$, since it is a forward trace, and to 
$\tName_2;\trace''$; indeed, $\trace_1$ and $\trace_2$ are coinitial and cofinal by hypothesis, thus
also $\tName_1;\trace'$ and $\tName_2;\trace''$ are coinitial and cofinal. We then have that 
$\tName_2;\trace''$ has a shorter equivalent forward trace, and so $\trace_2$ has a shorter equivalent 
forward trace. We can conclude by induction.

\item \emph{$\tName_1$ backward and $\tName_2$ forward.} This case is similar to the previous one.

\item \emph{$\tName_1$ and $\tName_2$ forward.} We have two possibilities: they are concurrent or are not.
In the latter case, they should conflict on a thread,  say $\ptag{t}P$, that they both consume and store in different 
memories. Since the two traces are cofinal, there should be a transition $\tName_2'$ in $\trace_2$ creating the 
same memory as $\tName_1$. However no other thread $\ptag{t}P$ is ever created in $\trace_2$; hence, 
this is not possible. 
%
Therefore, we can assume that $\tName_1$ and $\tName_2$ are concurrent. Let $\tName_2'$ be the transition 
in $\trace_2$ creating the same memory of $\tName_1$. We have to prove that $\tName_2'$ is concurrent to all the previous transitions. This holds since no previous transition can remove one of the processes needed for triggering $\tName_2'$ 
and since forward transitions can never conflict on $t$. Thus we can repetitively apply Lemma~\ref{lemma:square} 
to derive a trace equivalent to $\trace_2$ where $\tName_2$ and $\tName_2'$ are consecutive. We can apply a similar transformation to $\trace_1$. Now, we can apply Lemma~\ref{lemma:square} to $\tName_1$ and $\tName_2$ to have two traces of the same length as before but where the first pair of different transitions is closer to the end. We then conclude by inductive hypothesis.


\item \emph{$\tName_1$ and $\tName_2$ backward.} Let $m_1$ be the memory removed by $\tName_1$, which is surely different from the memory removed by $\tName_2$ (indeed, the two backward transitions cannot remove the same memory). Since the two traces are cofinal, either there is another transition in $\trace_1$ putting back the memory or there is a transition $\tName_1'$ in $\trace_2$ removing the same memory. In the first case, $\tName_1$ is concurrent to all the backward transitions following it, but the ones that consume processes generated by it. Thus, all such transitions have to be undone by corresponding forward transitions (since they are not possible in $\trace_2$). Consider the last such transition: we can use Lemma~\ref{lemma:square}  to make it the last backward transition. The forward transition undoing it should be concurrent to all the previous forward transitions (the reason is the same as in the previous case). We can then use Lemma~\ref{lemma:square} to make it the first forward transition. Finally, we can apply $\inver{\tName}\,;\,\tName \ \causallyEq\ \emptytrace{\target{\tName}}$ to remove the two transitions, thus shortening the trace. In this way, we obtain the thesis by inductive hypothesis.
\end{itemize}
\vspace*{-1.3cm}
\end{proof}

