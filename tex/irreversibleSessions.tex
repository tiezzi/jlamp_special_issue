% !TEX root = ../resPi_JLAMP.tex
\section{Commitable sessions}
\label{sec:irrev_calculus}

The calculus \respi\ discussed so far is \emph{fully} reversible, i.e. backward computations are always enabled. 
Full reversibility provides theoretical foundations for studying reversibility in session-based \pic, 
but it is not suitable for a practical use on structured communication-based programming. 
%
Therefore, in this section, we enrich the framework to allow computation to go backward and
forward along a session, allowing the involved parties to try different interactions, until the session 
is successfully completed. This is achieved by adding a specific action to the calculus for irreversibly committing 
the closure of sessions. 

It is worth noticing that the fully reversible characterisation of the calculus permits proving 
that its machinery for reversibility (i.e., memories and their usage) soundly works  with 
respect to the expected properties of a reversible calculus. This  remains valid also for 
the extension proposed here. In fact, as clarified below, the extended calculus basically
prunes some computations allowed in \respi, which corresponds to backward and forward
actions that are undesired after a session closure.

\subsection{\respi\ with commit}
The syntax of \respic\ (\emph{Reversible Session-based \pic\ with Commit}) 
is obtained from that of \respi\ (given in Figure~\ref{fig:syntax_respi}) by extending
the syntactic category of processes $P$ with process $\commitAct{k}{P}$, 
and by extending the syntactic category of memories $m$ with the \emph{commit memory}
$\commem{t_1}{s}{t_2}$. This new memory simply registers the closing event of the session
identified by $s$ due to an agreement of threads tagged by $t_1$ and $t_2$.

The irreversible closure of a session is achieved by the synchronisation on its session channel $s$ 
of two threads of the form  \mbox{$\ptag{t_1}\commitAct{\ce{s}}{P_1}$} and 
$\ptag{t_2}\commitAct{s}{P_2}$. 
%
This synchronisation acts similarly to the `cut' operator in Prolog, as both mechanisms 
are used to prevent unwanted backtracking. 
%
After the synchronisation, since the session $s$ is closed,
the continuations $P_1$ and $P_2$ %should not use any more 
can no longer use the session channel $s$; this 
check is statically enforced by the type system for \respic\ (presented later on).  

Formally, the semantics of \respic\ is obtained by adding the following rule to those 
defining the reduction relation of \respi\ (Figure~\ref{fig:reduction_respi}): 
\begin{center}
\begin{tabular}{@{}l@{\hspace*{.2cm}}r@{\ }l@{}}
	$\ptag{t_1}\commitAct{\ce{k}}{P_1} \ \mid \ \ptag{t_2}\commitAct{k}{P_2}$
	& $(k=s\ \, \text{or}\ \, k=\ce{s})$ &\rulelabel{commit}
	\\[.1cm]
	\quad $\fwred\ \  \res{t_1',t_2'}(
	\ptag{t_1'}P_1 \mid \ptag{t_2'}P_2
	\mid \commem{t_1}{s}{t_2})$	
\end{tabular}
\end{center}
Since $\texttt{commit}$ is an irreversible action that will never be backtracked, there is 
no need to remember information about the continuation processes in the generated 
memory. For the same reason, there is no backward rule inverse to \rulelabel{commit}. 

For what concerns the type discipline, types $\alpha$ (defined in Table~\ref{fig:typeSyntax}) 
are extended with type $\commitType$, while the typing system is extended with the following rule:  
$$
	\infer[$\ \rulelabel{Commit}$]{\basis;\sorting \judge \commitAct{k}{P} \hasType \typing\comp k:\commitType}
	{\basis;\sorting \judge P \hasType \typing\comp k:\inactType}
$$		
which ensures that after the commit the session is closed. 


\subsection{Irreversibility propagation}
When a $\texttt{commit}$ action is executed, all actions that caused it became unbacktrackable, 
although they were themselves reversible. In other words, a $\texttt{commit}$ action
creates a \emph{domino effect} that disables the possibility of reversing the session actions
previously performed.

To formalise the domino effect caused by the $\texttt{commit}$ irreversible action, we
have to introduce the following notions of \emph{head} and \emph{tail} of memories:
$$
\begin{array}{l@{\qquad\quad}l}
\mhead{\amem{\actEv{t_1}{\storedAct}{t_2}}{t_1'}{t_2'}} \, = \, \{t_1,t_2\}
&
\mtail{\amem{\actEv{t_1}{\storedAct}{t_2}}{t_1'}{t_2'}} \, = \, \{t_1',t_2'\}
\\[.2cm]
\mhead{\cmem{t}{\ifEv{e}{P}{Q}}{t'}} \, = \, \{t\}
&
\mtail{\cmem{t}{\ifEv{e}{P}{Q}}{t'}} \, = \, \{t'\}
\\[.2cm]
\mhead{\fmem{\fev{t}{t_1}{t_2}}} \, = \, \{t\}
&
\mtail{\fmem{\fev{t}{t_1}{t_2}}} \, = \, \{t_1,t_2\}
\\[.2cm]
\mhead{\commem{t_1}{s}{t_2}} \, = \, \{t_1,t_2\}
&
\mtail{\commem{t_1}{s}{t_2}} \, = \, \emptyset
\end{array}
$$  

Using the terminology of \cite{DanosK04}, we say that a memory is \emph{locked}
when the event stored inside can never be reverted, because the conditions
triggering the corresponding backward reduction will never be satisfied. 
Specifically, to perform a backward reduction is required the coexistence 
of a memory with threads properly tagged, and the latter will never be available 
due to an irreversible action. Let us now formalise, given a process $M$, 
its set $\lockedmemset{M}$ of locked memories.

\begin{definition}[Locked memories]\label{def:locked}
Let $M$ be a \respic\ process and $\memset{M}$ stand for the set of memories 
occurring in $M$. 
%
The set $\lockedmemset{M}$ of locked memories of $M$ is defined as follows:
\begin{itemize}
\item $\commem{t_1}{s}{t_2} \in \memset{M} 
\quad \Rightarrow \quad 
\commem{t_1}{s}{t_2} \in \lockedmemset{M}$
\\[-.25cm]
\item $m \in \lockedmemset{M}$, $m' \in \memset{M}$, $t\in \mhead{m}$, $t\in \mtail{m'}
\quad \Rightarrow \quad 
m' \in \lockedmemset{M}$
\end{itemize}
\end{definition}
The first point says that a commit memory is locked, while the second point describes
the propagation of the locking effect, i.e. the event within $m$ depends on the event within $m'$
(because the latter generates a thread involved in the former) and hence also $m'$ is locked.
Of course, $\lockedmemset{M} \subseteq \memset{M}$.

Now, we can demonstrate the main result about \respic, stating that committed sessions
cannot be reverted (Theorem~\ref{th:committedSessions}). 
This result is based on the notion of reversible memory (Definition~\ref{def:reversion}) 
and on Lemma~\ref{lemma:memoryIrreversibility}, ensuring that locked memories 
cannot be reverted.  We use $\bwred^+$ to denote the transitive closure of $\bwred$.

\begin{definition}\label{def:reversion}
Let $M$ be a \respic\ process. A memory $m\in \memset{M}$ is \emph{reversible} 
if there exists a process $M'$ such that  $M \bwred^+ M'$ and $m\notin\memset{M'}$.
\end{definition}
%
Intuitively, a memory can be reverted if there exists a backward computation that 
consumes it to restore the threads it memorises. 

\begin{lemma}\label{lemma:memoryIrreversibility}
Let $M$ be a \respic\ process. If $m\in\lockedmemset{M}$ then $m$ is not reversible.
\end{lemma}
\begin{proof}
The proof proceeds by contradiction (see \ref{app:irrev_calculus}). 
\end{proof}


\begin{theorem}\label{th:committedSessions}
Let $M$ be a \respic\ process and $s$ a session committed in $M$. 
Then, all interactions performed along $s$ cannot be reverted.
\end{theorem}
\begin{proof}
The proof relies on Lemma~\ref{lemma:memoryIrreversibility} (see \ref{app:irrev_calculus}). 
\end{proof}

%\todo{\scriptsize Move to Concluding Remarks?\\}
%It is worth noticing that \texttt{commit} must be carefully used in case of 
%subordinate sessions. For example, in the typical three-party scenario
%where a \emph{Customer} sends an order to a \emph{Seller} that, in his own turn, 
%contacts a \emph{Shipper} for the delivery, we have that the session between the
%\emph{Shipper} and the \emph{Seller} is subordinated to the session between
%the \emph{Customer} and the \emph{Seller}. Now, when the main session is 
%committed, also the subordinate session is involved in the commit, This is usually
%desirable, because the commit acts on the whole transaction and, hence, after 
%the commit the interaction with the \emph{Shipper} cannot be reverted. 
%However, if the subordinate session is previously committed, the main session 
%is affected because interaction performed before the commit cannot be reverted. 
%This latter situation is typically undesired; therefore, as a best practice, \texttt{commit}
%should be not used by subordinate sessions.  
  
% \subsection{Multiple providers scenario: \respic}
\subsection{The multiple providers scenario in \respic}
\label{sec:examplerespic}
Let us consider again the multiple providers scenario 
specified in \respi\ in Section~\ref{sec:examplerespi}. 

Suppose now that, for the sake of simplicity, the client and the first provider 
commit the session immediately after the acceptance of the quote. That is,
$P_{acc}$ and $Q_{acc}$ stand for $\commit{x}$ and $\commit{y}$,
respectively.
%
Thus, the computation described in Section~\ref{sec:example} in \respic\ corresponds to:
$$
\begin{array}{l@{\ }l}
\multicolumn{2}{l}{
\ptag{t_1}P_{client}  \ \mid \ \ptag{t_2}P_{provider1}  \ \mid \ \ptag{t_3}P_{provider2}  
}
\\[.1cm]
\multicolumn{2}{l}{
\fwred\ \fwred\ \fwred\ \fwred\ \fwred
}
\\[.1cm]
M \ = & \res{s,t_1^1,t_2^1,\ldots,t_1^5,t_2^4}
\\
&
(\,\ptag{t_1^5}\commit{\ce{s}} \ \mid \
\ptag{t_2^4}\commit{s} 
\\
&
\ \ \mid \
\amem{\actEv{t_1}{a_{login}\ldots}{t_2}}{t_1^1}{t_2^1}
\ \mid \ \ldots \ \mid \ 
\amem{\actEv{t_1^4}{\select{s}{l_{acc}}\ldots}{t_2^3}}{t_1^5}{t_2^4}
\,)
\\
&
\mid \ P_{provider2}  
\end{array}
$$
$M$ can now evolve as follows:
$$
\begin{array}{@{}l@{\, }l}
M \fwred\, M' \, = & \res{s,t_1^1,t_2^1,\ldots,t_1^5,t_2^4,t_1^6,t_2^5}
\\
&
(\,\ptag{t_1^6}\inact \ \mid \
\ptag{t_2^5}\inact
\ \mid\ \commem{t_1^5}{s}{t_2^4}
\\
&
\ \ \mid \
\amem{\actEv{t_1}{a_{login}\ldots}{t_2}}{t_1^1}{t_2^1}
\ \mid \ \ldots \ \mid \ 
\amem{\actEv{t_1^4}{\select{s}{l_{acc}}\ldots}{t_2^3}}{t_1^5}{t_2^4}
\,)
\\
&
\mid \ P_{provider2}  
\end{array}
$$
The memory $\commem{t_1^5}{s}{t_2^4}$ generated by this last forward reduction
is locked (first point of Definition~\ref{def:locked}). 
Thus, also memory $\amem{\actEv{t_1^4}{\select{s}{l_{acc}}\ldots}{t_2^3}}{t_1^5}{t_2^4}$
is locked, since its tail contains tags belonging to the head of the commit memory  
(second point of Definition~\ref{def:locked}). Repeatedly  applying the second point of 
Definition~\ref{def:locked}, we obtain that $\lockedmemset{M'} = \memset{M'}$, i.e.
all memory of $M'$ are locked. This means that $M'\not\bwred$ and hence, as desired, the 
computation along session $s$ cannot be reverted.



