% !TEX root = ../resPi_JLAMP.tex
\section{Properties of the reversible calculus}
\label{sec:properties}

We present in this section some properties of \respi, which are typically enjoyed 
by reversible calculi. We exploit terminology, arguments and proof techniques of previous works
on reversible calculi (in particular, \cite{LaneseMS10,DanosK04,CristescuKV13}).
%
As a matter of notation, we will use $\picSet$ and $\respiSet$
to denote the set of \pic\ processes and of \respi\ processes, respectively.
%
%Notation $\tuple{\cdot}$ stands for tuples, e.g. $\tuple{h}$ means $h_1,\ldots,h_n$.


\subsection{Correspondence with \pic}

We first show that \respi\ is a conservative extension of the (session-based) \pic.
In fact, as most reversible calculi, \respi\ is only a decoration of its host calculus. 
Such decoration can be erased by means of the \emph{forgetful map} 
$\forgetMap$, which maps \respi\ terms into 
\pic\ ones by simply removing memories, tag annotations and tag restrictions. 

\begin{definition}[Forgetful map]\label{def:forgetfulMap}
The forgetful map $\forgetMap\ :\ \respiSet \rightarrow \picSet$, 
mapping a \respi\ process $M$ into a \pic\ process $P$,
is inductively defined on the structure of $M$ as follows:
$$
\begin{array}{r@{\ }c@{\ }l@{\qquad\qquad}r@{\ }c@{\ }l}
\forget{\ptag{t}P} & = & P
&
\forget{\res{c}N} & = & \res{c}\forget{N}
\\[.1cm]
\forget{\res{t}N} & = & \forget{N}
&
\forget{N_1 \mid N_2} & = & \forget{N_1}\mid\forget{N_2}
\\[.1cm]
\forget{m} & = & \inact
&
\forget{\nil} & = & \inact\\[.1cm]
\end{array}
$$
\end{definition}

To prove the correspondence between \respi\ and \pic, we need the following 
auxiliary lemma relating structural congruence of \respi\ to that of \pic.

\begin{lemma}\label{lem:str_preserved}
Let $M$ and $N$ be two \respi\ processes. If $M \congr N$ then $\forget{M} \congr \forget{N}$.
\end{lemma}
\begin{proof}
We proceed by induction on the derivation of $M \congr N$
(see \ref{app:Correspondence}). 
%For most of the laws 
%in Figure~\ref{fig:congruence_respi} the conclusion is trivial because 
%$\forget{M} \congr \forget{N}$ directly corresponds to a law for \pic\ in Figure~\ref{fig:congruence_pic}.
%Instead, for the eighth and twelve laws, we easily conclude because by applying $\forgetMap$
%we obtain the identity. 
\end{proof}

Now, we can show that each forward reduction of a \respi\ process corresponds to 
a reduction of the corresponding \pic\ process.

\begin{lemma}\label{lem:correspondence}
Let $M$ and $N$ be two \respi\ processes. If $M \fwred N$ then $\forget{M} \red \forget{N}$.
\end{lemma}
\begin{proof}
We proceed by induction on the derivation of $M \fwred N$ (see \ref{app:Correspondence}). 
%Base cases:
%\begin{itemize}
%\item \rulelabel{fwCon}: We have that 
%$M \ = \ \ptag{t_1}\requestAct{a}{x}{P_1}\ \mid \ \ptag{t_2}\acceptAct{a}{y}{P_2}$
%\ \ and \ \ 
%$N \ = \ \res{s,t_1',t_2'}(
%	\ptag{t_1'}P_1\subst{\ce{s}}{x} \mid \ptag{t_2'}P_2\subst{s}{y}
%	\mid \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'})$
%with $s,\ce{s} \notin \freese{P_1,P_2}$. By definition of $\forgetMap$, we obtain
%$\forget{M} \ = \ \requestAct{a}{x}{P_1}\ \mid \ \acceptAct{a}{y}{P_2}$.
%Now, by applying rules \rulelabel{Con} and \rulelabel{Str}, we have
%$\forget{M} \red \res{s}(P_1\subst{\ce{s}}{x} \mid P_2\subst{s}{y} \mid \inact)\ =\ P$.
%By definition of $\forgetMap$, we have $\forget{N}=P$ that permits to conclude.
%\item \rulelabel{fwCom},\rulelabel{fwLab},\rulelabel{fwIf1},\rulelabel{fwIf2}: 
%These cases are similar to the previous one.
%\end{itemize}
%Inductive cases:
%\begin{itemize}
%\item \rulelabel{fwPar}: We have that 
%$M \ = \ M_1 \mid \ M_2$
%\ \ and \ \
%$N \ = \ M_1' \mid \ M_2$.
%By the premise of rule \rulelabel{fwPar}, we also have $M_1 \ \fwred\ M_1'$ from which, 
%by induction, we obtain
%$\forget{M_1} \ \red\ \forget{M_1'}$.
%By definition of $\forgetMap$, we get
%$\forget{M}\ = \ \forget{M_1} \mid \ \forget{M_2}$.
%By applying rule \rulelabel{Par}, we have
%$\forget{M} \red \forget{M_1'} \mid \ \forget{M_2} \ =\ P$.
%Thus, by definition of $\forgetMap$, we have
%$\forget{N} \ = \ P$ that directly permits concluding.
%\item \rulelabel{fwRes}: 
%This case is similar to the previous one; in particular, when the restricted name is a tag, 
%it is not even necessary to apply rule \rulelabel{Res}, because the forgetful map erases 
%the restriction.
%\item \rulelabel{fwStr}: 
%By the premise of rule \rulelabel{fwStr}, we have 
%$M  \congr M'$,
%$M' \fwred N'$
%and $N'  \congr N$.
%By induction, we obtain
%$\forget{M'} \red \forget{N'}$. 
%By applying Lemma~\ref{lem:str_preserved},
%we have $\forget{M}  \congr \forget{M'}$ and $\forget{N'} \congr \forget{N}$
%that allow us to conclude.
%\end{itemize}
\end{proof}


The correspondence between \respi\ and \pic\ reductions is completed by 
the following lemmas, which intuitively are the inverse of 
Lemma~\ref{lem:str_preserved} and
Lemma~\ref{lem:correspondence}.

\begin{lemma}\label{lem:str_preserved_inv}
Let $P$ and $Q$ be two \pic\ processes. If $P \congr Q$ then for 
any \respi\ process $M$ such that $\forget{M}=P$ there exists a \respi\ process
$N$ such that $\forget{N}=Q$ and $M \congr N$.
\end{lemma}
\begin{proof}
The proof is straightforward (see \ref{app:Correspondence}). 
%Indeed, given a \respi\ process $M$ such that $\forget{M}=P$,
%it must have the form $\res{\tuple{t}}(\ptag{t}P \mid \prod_{i\in I}m_i)$ up to $\congr$. Thus, 
%the process $N$, such that $\forget{N}=Q$, can be defined accordingly: 
%$N \congr \res{\tuple{t}}(\ptag{t}Q \mid \prod_{i\in I}m_i)$. Now, we can conclude 
%by exploiting the ninth law in Figure~\ref{fig:congruence_respi}, 
%i.e. $\ptag{t}P \congr \ptag{t}Q$\ \ if $P \congr Q$, and the fact that relation $\congr$ on
%\respi\ processes is a congruence. 
\end{proof}


\begin{lemma}\label{lem:correspondence_inv}
Let $P$ and $Q$ be two \pic\ processes. If $P \red Q$ then 
for any \respi\ process $M$ such that $\forget{M}=P$ there exists a \respi\ process
$N$ such that $\forget{N}=Q$ and $M \fwred N$.
\end{lemma}
\begin{proof}
We proceed by induction on the derivation of $P \red Q$ (see \ref{app:Correspondence}). 
%Base cases:
%\begin{itemize}
%\item \rulelabel{Con}: We have that 
%$P \, = \, \requestAct{a}{x}{P_1} \mid \acceptAct{a}{y}{P_2}$
% \ and\ \
%$Q \, = \, \res{s}(P_1\subst{\ce{s}}{x} \mid P_2\subst{s}{y})$ 
%with $s,\ce{s} \notin \freese{P_1,P_2}$.
%Let $M$ be a \respi\ process such that $\forget{M}=P$, it must have the form 
%$\res{\tuple{t}}(\ptag{t_1}\requestAct{a}{x}{P_1} \mid \ptag{t_2}\acceptAct{a}{y}{P_2} \mid \prod_{i\in I}m_i)$ up to $\congr$. Thus, by applying rules \rulelabel{fwCon}, \rulelabel{fwPar}, \rulelabel{fwRes} and \rulelabel{fwStr}, 
%we get $M \fwred \res{\tuple{t},s,t_1',t_2'}(
%\ptag{t_1'}P_1\subst{\ce{s}}{x} \mid \ptag{t_2'}P_2\subst{s}{y}
%\mid \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'} \mid \prod_{i\in I}m_i) \, =\, N$.
%We conclude by applying $\forgetMap$ to $N$, since we obtain $\forget{N}=Q$.
%\item \rulelabel{Com},\rulelabel{Lab},\rulelabel{If1},\rulelabel{If2}: 
%These cases are similar to the previous one.
%\end{itemize}
%Inductive cases:
%\begin{itemize}
%\item \rulelabel{Par}: We have that 
%$P \ = \ P_1 \mid P_2$
%\ \ and \ \
%$Q \ = \ P_1' \mid P_2$.
%Let $M$ be a \respi\ process such that $\forget{M}=P_1 \mid P_2$. 
%We have $M \congr \res{\tuple{t}}(\ptag{t_1}P_1 \mid \ptag{t_2}P_2 \mid  \prod_{i\in I}m_i)
%\congr \res{\tuple{t'}}(M_1 \mid \ptag{t_2}P_2 \mid  \prod_{j\in J}m_j)$
%with $M_1\congr  \res{\tuple{t''}}(\ptag{t_1}P_1 \mid \prod_{k\in K}m_k)$,
%$\tuple{t}=\tuple{t'},\tuple{t''}$ and $J\cup K=I$.
%By the premise of rule \rulelabel{Par}, we also have $P_1 \red P_1'$ from which, 
%by induction, since $\forget{M_1}=P_1$, 
%there exists $M_1'$ such that $\forget{M_1'}=P_1'$ and $M_1 \fwred M_1'$.
%Thus, by applying rules \rulelabel{fwPar}, \rulelabel{fwRes} and \rulelabel{fwStr}, 
%we get 
%$M \fwred  \res{\tuple{t'}}(M_1' \mid \ptag{t_2}P_2 \mid  \prod_{j\in J}m_j)\,=\,N$.
%We conclude by applying $\forgetMap$ to $N$, because
%$\forget{N}=\forget{M_1'} \mid P_2= P_1' \mid P_2 =Q$.
%\item \rulelabel{Res}: 
%This case is similar to the previous one.
%\item \rulelabel{Str}: We have that 
%$P \congr P'$, $Q \congr Q'$ and $P' \red Q'$.
%Let $M$ be a process such that $\forget{M}=P$.
%By applying Lemma~\ref{lem:str_preserved_inv}, 
%there exists $M'$ such that $\forget{M'}=P'$ and $M \congr M'$.
%By induction, there is $N'$ such that $\forget{N'}=Q'$ and $M' \fwred N'$.
%By applying Lemma~\ref{lem:str_preserved_inv} again, 
%there exists $N$ such that $\forget{N}=Q$ and $N \congr N'$.
%By applying rule \rulelabel{fwStr}, we conclude $M \fwred N$.
%\end{itemize}
\end{proof}





\subsection{Loop lemma}
The following lemma shows that, in \respi, backward reductions are the inverse 
of the forward ones and vice versa.  

\begin{lemma}[Loop lemma]\label{lemma:loop}
Let $M$ and $N$ be two reachable \respi\ processes. 
$M \fwred N$ if and only if $N \bwred M$.
\end{lemma}
\begin{proof}
The proof for the \emph{if} part is by induction on the derivation of $M \fwred N$, %.
%Base cases:
%\begin{itemize}
%\item \rulelabel{fwCon}: We have that 
%$M \ = \ \ptag{t_1}\requestAct{a}{x}{P_1}\ \mid \ \ptag{t_2}\acceptAct{a}{y}{P_2}$
%\ \ and \ \ 
%$N \ = \ \res{s,t_1',t_2'}(
%	\ptag{t_1'}P_1\subst{\ce{s}}{x} \mid \ptag{t_2'}P_2\subst{s}{y}
%	\mid \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'})$
%with $s,\ce{s} \notin \freese{P_1,P_2}$. 
%By applying rule \rulelabel{bwCon}, we can directly conclude 
%$N \bwred M$.
%\item \rulelabel{fwCom},\rulelabel{fwLab},\rulelabel{fwIf1},\rulelabel{fwIf2}: 
%These cases are similar to the previous one.
%\end{itemize}
%Inductive cases:
%\begin{itemize}
%\item \rulelabel{fwPar}: We have that 
%$M  =  N_1 \mid N_2$, $N  = N_1' \mid N_2$ and $N_1 \fwred N_1'$.
%By induction $N_1' \bwred N_1$. Thus, we conclude by applying rule \rulelabel{bwPar},
%since we get $N  = N_1' \mid N_2 \bwred N_1 \mid N_2 = M$.
%\item \rulelabel{fwRes} and \rulelabel{fwStr}: 
%These cases can be proved by straightforwardly resorting to the induction hypothesis as in the previous case.
%\end{itemize}
%
%\noindent
%The proof for the \emph{only if} part is by induction on the derivation of $N \bwred M$.
while the proof for the \emph{only if} part is by induction on the derivation of $N \bwred M$
(see \ref{app:LoopLemma}).
%Base cases:
%\begin{itemize}
%\item \rulelabel{bwCon}: We have that 
%$N \ = \ \res{s,t_1',t_2'}(\ptag{t_1'}P \mid \ptag{t_2'}Q 
%	\mid \amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'})$
%\ \ and \ \ 
%$M \ = \ \ptag{t_1}\requestAct{a}{x}{P_1}\ \mid \ \ptag{t_2}\acceptAct{a}{y}{P_2}$.
%Since $N$ is a reachable process, memory $\amem{\actEv{t_1}{\initAct{a}{x}{y}{P_1}{P_2}{s}}{t_2}}{t_1'}{t_2'}$
%has been generated by a synchronisation between threads
%$\ptag{t_1}\requestAct{a}{x}{P_1}$ and $\ptag{t_2}\acceptAct{a}{y}{P_2}$,
%producing a session channel $s$ and two continuation processes 
%$P_1\subst{\ce{s}}{x}$ and $P_2\subst{s}{y}$ tagged by $t_1'$ and $t_2'$, respectively.
%Now, by tag uniqueness implied by reachability and use of restriction in tag generation,
%$P$ and $Q$ must coincide with $P_1\subst{\ce{s}}{x}$ and $P_2\subst{s}{y}$, respectively.
%Therefore, by applying rule \rulelabel{fwCon}, we can directly conclude $M \fwred N$.
%\item \rulelabel{bwCom},\rulelabel{bwLab},\rulelabel{bwIf}: 
%These cases are similar to the previous one.
%\end{itemize}
%Inductive cases:
%\begin{itemize}
%\item \rulelabel{bwPar}: We have that 
%$N  =  N_1 \mid N_2$, $M  = N_1' \mid N_2$ and $N_1 \bwred N_1'$.
%By induction $N_1' \fwred N_1$. Thus, we conclude by applying rule \rulelabel{fwPar},
%since we get $M  = N_1' \mid N_2 \fwred N_1 \mid N_2 = N$.
%\item \rulelabel{bwRes} and \rulelabel{bwStr}: 
%These cases can be proved by straightforwardly resorting to the induction hypothesis as in the previous case.
%\end{itemize}
\end{proof}



\subsection{Causal consistency}
We show here that reversibility in \respi\ is causally consistent. 
Informally, this means that an action can be reverted only after all the actions causally 
depending on it have already been reverted. In this way, in presence of independent actions, 
backward computations are not required to necessarily follow the exact execution order of 
forward computations in reverse. We formalise below the notions of independent (i.e., 
\emph{concurrent}) actions and of causal consistency.


As in \cite{LaneseMS10} and \cite{DanosK04}, we rely on the notion of transition.
%
In \respi, a \emph{transition} is a triplet of the form \mbox{$M \transition{m,\mSet,\fwred} N$}
(resp. \mbox{$M \transition{m,\mSet,\bwred} N$}), where 
$M$ and $N$ are closed reachable \respi\ processes such that
$M \fwred N$ (resp. $M \bwred N$), 
$m$ is the action or choice memory involved in the reduction, 
and $\mSet$ is the set of fork memories possibly involved in the reduction.
%
A memory is \emph{involved} in a reduction if it is created or removed by the reduction.
%
We use $\tlab$ to denote transition \emph{labels} $(m,\mSet,\fwred)$ and $(m,\mSet,\bwred)$.
If $\tlab$ is $(m,\mSet,\fwred)$, then its inverse is $\inver{\tlab}=(m,\mSet,\bwred)$
and vice versa. 
%
In a transition $M \transition{\tlab} N$, we call $M$ the \emph{source} of the transition 
and $N$ its \emph{target}. We use $\tName$ to range over transitions; $\inver{\tName}$
denotes the inverse of transition $\tName$.

Two transitions are \emph{coinitial} if they have the same source, 
\emph{cofinal} if they have the same target, \emph{composable} if the 
target of one is the source of the other. 
%A sequence of pairwise composable transitions is called a \emph{trace};
A sequence of transitions,  where each pair of sequential transitions is composable, is called a \emph{trace};
we use $\trace$ to range over traces.
Notions of target, source and composability extend naturally to traces.
We use $\emptytrace{M}$ to denote the empty trace with source $M$
and $\trace_1;\trace_2$ the composition of two composable traces $\trace_1$
and $\trace_2$.

We consider only transitions $M \transition{\tlab} N$ where $M$ and $N$ 
do not contain threads of the form $\ptag{t}(P\mid Q)$.
This condition can be always satisfied by splitting all threads of this kind into 
sub-threads, until their disappearance in the considered terms, using the structural
law $\ptag{t}(P\mid Q) \congr \res{t_1,t_2}(\ptag{t_1}P \mid \ptag{t_2}Q \mid \fmem{\fev{t}{t_1}{t_2}})$. 
Moreover, since conflicts between transitions are identified by means of tag identifiers
(see Definition~\ref{def:concurrentTrasitions} below), we only consider transitions
that do not use $\alpha$-conversion on tags, and that generates fork memories in a 
deterministic way, e.g. given a memory $\fmem{\fev{t}{t_1}{t_2}}$ tags $t_1$ and $t_2$
are generated by applying an injective function to $t$.

The \emph{stamp} $\stamp{\tlab}$ of a transition label $\tlab$ identifies the threads
involved in the corresponding transition, and is defined as follows (we use $\tagSet$
to denote a set of tags $\{t_i\}_{i \in I}$):
$$
\begin{array}{l}
\stamp{m,\mSet,\fwred} = \stamp{m,\mSet,\bwred} = \stamp{m}\cup\stampFork{\stamp{m}}{\mSet}
\\[.3cm]
\stamp{\amem{\actEv{t_1}{\storedAct}{t_2}}{t_1'}{t_2'}} = \set{t_1,t_2,t_1',t_2'}
\\[.3cm]
\stamp{\cmem{t}{\ifEv{e}{P}{Q}}{t'}} = \set{t,t'}
\\[.3cm]
\stampFork{\tagSet}{\set{m_i}_{i \in I}} = \bigcup_{i\in I}\stampFork{\tagSet}{m_i}
%\end{array}
%$$
\\[.2cm]
%$$
%\begin{array}{l}
\stampFork{\tagSet}{\fmem{\fev{t}{t_1}{t_2}}} = \left\{
\begin{array}{@{}ll}
\set{t_1,t_2} & \text{if } t\in \tagSet\\[.1cm]
\emptyset & \text{otherwise}
\end{array}\right.
\end{array}
$$
The stamp of fork memories permits to take into account 
the relationships between a thread and its sub-threads.
This is similar to the closure over tags used in \cite{GLMT}.
Notably, as in \cite{LaneseMS10}, the tags of 
continuation processes are inserted into a stamp, in order to 
take into account possible conflicts between a forward 
transition and a backward one.  
Notice also that it is instead not necessary to include 
in the stamp the fresh session channel created or used
by a reduction. In fact, this would allow to detect conflicts
between the transitions involving the memory corresponding to
the creation of the channel and the transitions where such channel 
is used. Such conflicts, however, are already implicitly considered,
since after its creation the channel is only known by the threads 
corresponding to the continuation processes, which are already 
considered in the stamp as discussed above.

We can now define when two transitions are concurrent.
\begin{definition}[Concurrent transitions]\label{def:concurrentTrasitions}
Two coinitial transitions $M \transition{\tlab_1} M_1$
and $M \transition{\tlab_2} M_2$ are \emph{in conflict} if 
%\begin{itemize}
%\item $\exists\ t \in \stamp{\tlab_1}$ such that $t \in \stamp{\tlab_2}$, or
%\item $\exists\ t \in \stamp{\tlab_2}$ such that $t \in \stamp{\tlab_1}$.
%\end{itemize}
$\stamp{\tlab_1} \cap \stamp{\tlab_2} \neq \emptyset$.
Two coinitial transitions are \emph{concurrent} if they are not in conflict.  
\end{definition}
Intuitively, two transitions are concurrent when they do not involve a common thread. 


The following lemma characterises the causally independence among
concurrent transitions.

\begin{lemma}[Square lemma]\label{lemma:square}
If $\tName_1=M\transition{\tlab_1}M_1$ and $\tName_2=M\transition{\tlab_2}M_2$
are two coinitial concurrent transitions, then there exist two cofinal transitions
$\tName_2/\tName_1=M_1\transition{\tlab_2}N$ and $\tName_1/\tName_2=M_2\transition{\tlab_1}N$.
\end{lemma}
\begin{proof} %\emph{(Sketch)}\ \
By case analysis on the form of transitions $\tName_1$ and $\tName_2$
(see \ref{app:causality}).
\end{proof}

In order to study the causality of \respi\ reversibility, we introduce the notion 
of \emph{causal equivalence} \cite{Levy76,DanosK04} between traces, denoted $\causallyEq$.
This is defined as the least equivalence relation between traces closed under 
composition that obeys the following rules:
$$
\tName_1\,;\,\tName_2/\tName_1 \ \causallyEq \ \tName_2\,;\,\tName_1/\tName_2
\qquad\qquad
\tName\,;\,\inver{\tName} \ \causallyEq\ \emptytrace{\source{\tName}}
\qquad\qquad
\inver{\tName}\,;\,\tName \ \causallyEq\ \emptytrace{\target{\tName}}
$$
Intuitively, the first rule states that the execution order of two concurrent transitions
can be swapped, while the other rules state that the composition of a trace with its inverse 
is equivalent to the empty transition.
 
Now, we conclude with the result stating that two coinitial causally equivalent traces  
lead to the same final state. Thus, in such case, we can rollback to the initial state by 
reversing any of the two traces. 

\begin{theorem}[Causal consistency]\label{th:causalConsistency}
Let $\trace_1$ and $\trace_2$ be coinitial traces. Then, $\trace_1 \causallyEq \trace_2$
if and only if $\trace_1$ and $\trace_2$ are cofinal. 
\end{theorem}
\begin{proof}%\emph{(Sketch)}\ \
By construction of $\causallyEq$ and by applying Lemma~\ref{lemma:square} and 
other two auxiliary lemmas (see \ref{app:causality}).
\end{proof}
