% !TEX root = ../resPi_JLAMP.tex
\section{Introduction}
\label{sec:intro}

In the field of programming languages,
\emph{reversible computing} aims at providing a computational model that, besides 
the standard forward executions,  also permits backward execution steps to 
undo the effect of previously performed forward computations.
%
Despite being a subject of study %since 
for many years, reversible computing is recently 
experiencing a rise in popularity. This is mainly due to the fact that reversibility is
a key ingredient in different application domains.
%, e.g.~biological systems, hardware design, 
%software debugging and --for what specifically concerns our interest--  
In particular, for what specifically concerns our interest, 
many researchers have put forward exploiting this paradigm in the
design of reliable concurrent systems. In fact, it permits understanding existing patterns 
for programming reliable systems (e.g., compensations, checkpointing, transactions) and, 
possibly, developing new ones.

A promising line of research on this topic advocates reversible variants of 
well-established process calculi, such as CCS~\cite{CCS} and \pic~\cite{PICALC}, as formalisms for 
studying reversibility mechanisms in concurrent systems. 
%
By pursing this line of research, in this work we incorporate reversibility into a variant 
of \pic\ equipped with \emph{session} primitives supporting communication-based 
programming. 
%
A (binary) session consists in a series of reciprocal interactions between two parties, 
possibly with branching and recursion. Interactions on a session are performed via a 
dedicated private channel, which is generated when initiating the session. 
%
Session primitives come together with a session type 
discipline offering a simple checking framework to statically guarantee the correctness of 
communication patterns. This prevents programs from interacting according to incompatible 
patterns.

Practically, combining reversibility and sessions paves the way for the development of 
session-based communication-centric distributed software intrinsically capable of performing 
reversible computations. In this way, without further coding effort by the 
application programmer, the interaction among session parties is relaxed so that, e.g.,  
the computation can automatically go back, thus allowing to take different paths
when the current one is not satisfactory.  
%
As an application example, used in this paper for illustrating our approach, 
we consider a simple scenario involving 
a client and multiple providers offering 
the same service (e.g., on-demand video streaming). 
The client connects to a provider to request a given service  
(specifying, e.g., title of a movie, video quality, etc.). 
The provider replies with a quote determined according to the requested 
quality of service and to the servers status (current load, available bandwidth, etc.). 
Then, the client can either accept, negotiate or reject the quote; 
in the first two cases, the interaction between the two parties shall continue. 
%
If a problem occurs during the interaction between the client and the provider 
for finalising the service agreement, the computation can be automatically reverted. 
This allows the client to partially undo the current session, in order to take a different 
computation path along the same session, or even start a new session with (possibly) 
another provider.

The proposed reversible session-based calculus, called \respi\ (\emph{Reversible Session-based} \pic), 
relies on memories to store information about 
interactions and their effects on the system, which otherwise would be lost during forward 
computations. This data is used to enable backward computations that revert the effects of 
the corresponding forward ones. Each memory is devoted to record data concerning a single 
event, which can correspond to the taking place of a communication action, a choice or a thread 
forking. Memories are connected with one other, in order to keep track of the computation history, 
by using unique thread identifiers as links.
%
Like all other formalisms for reversible computing in concurrent settings, 
forward computations are undone in a \emph{causal-consistent} fashion \cite{Levy76,DanosK04}. 
This means that backtracking does not have to necessarily follow the exact order of forward 
computations in reverse, because independent actions can be undone in a different order. 
Thus, an action can be undone only after all the actions causally depending on it have already been 
undone.

Concerning the session type discipline, \respi\ inherits the notion of types and the typing system
from \pic. Thus, the related results are mainly based on the ones stated for \pic. Besides 
the possibility of taking advantage of the theory already defined for \pic, this also allows our investigation to focus on a standard session type setting, rather than on an ad-hoc one
specifically introduced for our calculus.  

The resulting formalism offers a theoretical basis for examining the interplay between reversible 
computations and session-based structured interactions.  
%
We notice that reversibility enables session parties not only 
to partially undo the interactions performed along the current session, but also to automatically undo 
the whole session and restart it, possibly involving different parties. The advantage of the reversible 
approach is that this behaviour is realised without explicitly implementing loops, but simply relying on the
reversibility mechanism available in the language semantics. 
%
On the other hand, the session type discipline affects reversibility as it forces concurrent interactions 
to follow structured communication patterns. 
If we would consider only a single session, due to linearity, a causal-consistent form of reversibility would 
not be necessary, i.e.~concurrent interactions along the same session are forbidden 
and, hence, the rollback would follow a single path. Instead, in the general case, concurrent interactions 
along different sessions may take place, thus introducing causal dependences. In this case, a session 
execution has to be reverted in a causal-consistent fashion. 
%
Notably, interesting issues concerning reversibility and session types are still open questions, 
especially for what concerns the validity in the reversible setting of standard properties 
(e.g., progress enforcement) and possibly new properties (e.g., reversibility of ongoing session history, 
safe closure of subordinate sessions).  

It is worth noticing that the proposed calculus is \emph{fully} reversible, i.e.~backward computations are always enabled. 
Full reversibility provides theoretical foundations for studying reversibility in session-based \pic, 
but it is not suitable for a practical use on structured communication-based programming. 
In fact, reverting a completed session might not be desirable. Therefore, we also propose an extension 
of the calculus with an \emph{irreversible} action for committing the closure of sessions. 
In this way, computation would go backward and forward, allowing the parties to try different 
interactions, until the session is successfully completed and, hence, irreversibly closed.



\paragraph{Summary of the rest of the paper}
Section~\ref{sect:relatedWork} reviews strictly related work. 
%
Section~\ref{sec:host_calculus} recalls syntax and semantics definitions 
of the considered session-based variants of \pic. 
%
Section~\ref{sec:rev_calculus} introduces \respi, our reversible session-based calculus.
%
Section~\ref{sec:properties} shows the results concerning the reversibility properties of \respi.
%
Section~\ref{sec:types} describes the related typing discipline.
%
Section~\ref{sec:irrev_calculus} presents the extension of \respi\ with irreversible commit actions.
%
Section~\ref{sec:conclusions} concludes the paper
by touching upon directions for future work.
%
Proofs of results are collected in the Appendices. 
