% !TEX root = ../resPi_JLAMP.tex
\section{Type discipline}
\label{sec:types}

In this section, first we recall the session type discipline of session-based \pic\, 
then we discuss how we could exploit it to type \respi\ processes.

\subsection{Typing session-based \pic}
\label{sec:typingPic}

The type discipline presented here is basically the one proposed in \cite{YoshidaV07}, 
except for the notation of the calculus that has been revised according to~\cite{MostrousY09}.

\subsubsection{Types}
The syntax of \emph{sorts}, ranged over by $S$, $S'$, \ldots, and \emph{types}, ranged over by $\alpha$, 
$\beta$, \ldots, is defined in Figure~\ref{fig:typeSyntax}.
%
\begin{figure}[t]
	\begin{centering}
%	\begin{tabular}{rcl@{\hspace{-.1cm}}r}
%	\textbf{Sorts}\quad
%	$S$ & ::= & $\boolType$ & boolean\\
%	& $\mid$ & $\intType$ & integer\\
%	& $\mid$ & $\sharedChanType{\alpha}$ & shared channel\\
%	\\
%	\textbf{Types}\quad
%	$\alpha$ & ::= & $\outType{S}.\alpha$ & output\\	
%	& $\mid$ & $\inpType{S}.\alpha$ & input\\		
%	& $\mid$ & $\thrType{\beta}.\alpha$ & throw\\		
%	& $\mid$ & $\catType{\beta}.\alpha$ & catch\\			
%	& $\mid$ & $\selType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}$ & selection\\			
%	& $\mid$ & $\branchType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}$ & branching\\				
%	& $\mid$ & $\inactType$ & end
%	\\[.2cm]
%	\hline
%	\end{tabular}
	\begin{tabular}{rcl@{\hspace{-.1cm}}r}
	\textbf{Sorts}\quad
	$S$ & ::= & $\boolType$ & boolean\\
	& $\mid$ & $\intType$ & integer\\
	& $\mid$ & $\sharedChanType{\alpha}$ & shared channel\\
	\\
	\textbf{Types}\quad
	$\alpha,\beta$ & ::= & $\outType{S}.\alpha$ & output\\	
	& $\mid$ & $\inpType{S}.\alpha$ & input\\		
	& $\mid$ & $\thrType{\beta}.\alpha$ & throw\\		
	& $\mid$ & $\catType{\beta}.\alpha$ & catch\\			
	& $\mid$ & $\selType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}$ & selection\\			
	& $\mid$ & $\branchType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}$ & branching\\				
	& $\mid$ & $\inactType$ & end\\
	& $\mid$ & $t$ & type variable\\
	& $\mid$ & $\recType{t}.\alpha$ & recursion
	\\[.2cm]
	\hline
	\end{tabular}
	\vspace*{-.2cm}
	\caption{Syntax of sorts and types}
	\label{fig:typeSyntax}
	\end{centering}
	\vspace*{.5cm}
\end{figure}
%
The type $\outType{S}.\alpha$ represents the behaviour of first outputting a value of sorts $S$, 
then performing the actions prescribed by type $\alpha$.
Type $\thrType{\beta}.\alpha$ represents a similar behaviour, which starts with session output 
(throw) instead.
Types $\inpType{S}.\alpha$ and $\catType{\beta}.\alpha$ are the dual ones, 
receiving values instead of sending. 
%
Type $\branchType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}$ describes a branching 
behaviour: it waits with $n$ options, and behave as type $\alpha_i$ if the $i$-th action is selected 
(external choice).
Type $\selType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}$ represents the behaviour 
which would select one of $l_i$ and then behaves as $\alpha_i$, according to the selected $l_i$ 
(internal choice). Type $\inactType$ represents inaction, acting as the unit of sequential composition.
%
Type $\recType{t}.\alpha$ denotes a recursive behaviour, representing the behaviour that starts 
by doing $\alpha$ and, when variable $t$ is encountered, recurs to $\alpha$ again. 
%
As in \cite{YoshidaV07}, we take an \emph{equi-recursive} view of types, not distinguishing between 
a type $\recType{t}.\alpha$ and its unfolding $\alpha\subst{\recType{t}.\alpha}{t}$, and we are 
interested on \emph{contractive} types only, i.e. for each of sub-expressions 
$\recType{t}.\recType{t_1}\ldots\recType{t_n}.\alpha$  the body $\alpha$ is not $t$. 
%
The result is that, in a typing derivation, types $\recType{t}.\alpha$ and $\alpha\subst{\recType{t}.\alpha}{t}$ can be used interchangeably. 


For each type $\alpha$, we define $\dual{\alpha}$, the \emph{dual type} of $\alpha$, 
by exchanging $!$ and $?$, and $\&$ and $\oplus$. The inductive definition is in Figure~\ref{fig:dualType}.
%
\begin{figure}[t]
\small
	\begin{centering}
	\begin{tabular}{r@{\ }c@{\ }l@{\qquad}r@{\ }c@{\ }l@{\qquad}r@{\ }c@{\ }l}
	$\dual{\outType{S}.\alpha}$ & = & $\inpType{S}.\dual{\alpha}$
	&
	$\dual{\outType{\beta}.\alpha}$ & = & $\inpType{\beta}.\dual{\alpha}$	
	&
	$\dual{\selType{\branch{l_i}{\alpha_i}}_{i\in I}}$ & = & $\branchType{\branch{l_i}{\dual{\alpha_i}}}_{i\in I}$
	\\[.3cm]
	$\dual{\inpType{S}.\alpha}$ & = & $\outType{S}.\dual{\alpha}$	
	&
	$\dual{\inpType{\beta}.\alpha}$ & = & $\outType{\beta}.\dual{\alpha}$	
	&
	$\dual{\branchType{\branch{l_i}{\alpha_i}}_{i\in I}}$ & = & $\selType{\branch{l_i}{\dual{\alpha_i}}}_{i\in I}$	
	\\[.3cm]
	$\dual{\inactType}$ & = & $\inactType$
	&
	$\dual{\recType{t}.\alpha}$ & = & $\recType{t}.\dual{\alpha}$
	& 
	$\dual{t}$ & = & $t$
	\\[.2cm]
	\hline
	\end{tabular}
	\vspace*{-.2cm}
	\caption{Dual types}
	\label{fig:dualType}
	\end{centering}
\end{figure}
%

\subsubsection{Typing system}
A \emph{sorting} (resp. a \emph{typing}, resp. a \emph{basis}) is a finite partial map from 
shared identifiers to sorts (resp. from session identifiers to types, resp. from process variables to typings). 
We let $\sorting$, $\sorting'$, \ldots (resp. $\typing$, $\typing'$, \ldots, 
resp. $\basis$, $\basis'$, \ldots) range over sortings (resp. typings, resp. bases).
%
We write $\typing \comp k:\alpha$ when $k \notin dom(\typing)$;
this notation is then extended to $\typing \comp \typing'$. 

Typing judgements are of the form \mbox{$\basis;\sorting \judge P \hasType \typing$}
which stands for ``under the environment $\basis;\sorting$, process $P$ has typing $\typing$''. 
The typing system is defined by the axioms and rules in Figure~\ref{fig:typingSysytem}. 
We call a typing \emph{completed} when it contains only $\inactType$ types. 
A typing $\typing$ is called \emph{balanced} if  whenever $s:\alpha,\ce{s}:\beta\in \typing$, then $\alpha=\dual{\beta}$.
%
We refer the interested reader to \cite{YoshidaV07} for detailed comments on the rules.

\begin{figure}[!t]
\footnotesize
	\begin{centering}
%	\begin{tabular}{@{\hspace*{-2cm}}c}
%	$\sorting \judge \ctrue \hasType \boolType$\ \ \rulelabel{Bool$_{\mathit{tt}}$}
%	\qquad
%	$\sorting \judge \cfalse \hasType \boolType$\ \ \rulelabel{Bool$_{\mathit{ff}}$}
%	\qquad
%	$\sorting \judge 1 \hasType \intType$\ \ \rulelabel{Int}	
%	\qquad \ldots
%	\\[.4cm]
%	$
%	\infer[$\ \rulelabel{Sum}$]{\sorting \judge +(e_1,e_2) \hasType \intType}
%	{\sorting \judge e_1 \hasType \intType & & \sorting \judge e_2 \hasType \intType}
%	$		
%	\qquad
%	$
%	\infer[$\ \rulelabel{And}$]{\sorting \judge \wedge(e_1,e_2) \hasType \boolType}
%	{\sorting \judge e_1 \hasType \boolType & & \sorting \judge e_2 \hasType \boolType}
%	$		
%	\qquad \ldots
%	\\[.2cm]
%	\end{tabular}
%	\begin{tabular}{@{\hspace*{-2cm}}r@{\hspace*{.4cm}}r}
%	$\sorting\comp u:S \judge u \hasType S$\ \ \rulelabel{Id}	
%	&
%	\infer[$\ \rulelabel{Inact}$]{\basis;\sorting \judge \inact \hasType \typing}
%	{\typing\ \  \text{completed}}	
%	\\[.4cm]	
%	$
%	\infer[$\ \rulelabel{Req}$]{\basis;\sorting \judge \requestAct{u}{x}{P} \hasType \typing}
%	{\sorting \judge u \hasType \sharedChanType{\alpha} & & \basis;\sorting \judge P \hasType \typing\comp x:\dual{\alpha}}
%	$		
%	&
%	$
%	\infer[$\ \rulelabel{Acc}$]{\basis;\sorting \judge \acceptAct{u}{x}{P} \hasType \typing}
%	{\sorting \judge u \hasType \sharedChanType{\alpha} & & \basis;\sorting \judge P \hasType \typing\comp x:\alpha}
%	$		
%	\\[.4cm]	
%	$
%	\infer[$\ \rulelabel{Send}$]{\basis;\sorting \judge \sendAct{k}{e}{P} \hasType \typing\comp k:\outType{S}.\alpha}
%	{\sorting \judge e \hasType S & & \basis;\sorting \judge P \hasType \typing\comp k:\alpha}
%	$		
%	&
%	$
%	\infer[$\ \rulelabel{Thr}$]{\basis;\sorting \judge \sendAct{k}{k'}{P} \hasType \typing\comp k:\thrType{\alpha}.\beta\comp k':\alpha}
%	{\basis;\sorting \judge P \hasType \typing\comp k:\beta}
%	$		
%	\\[.4cm]	
%	$
%	\infer[$\ \rulelabel{Rcv}$]{\basis;\sorting \judge \receiveAct{k}{x}{P} \hasType \typing\comp k:\inpType{S}.\alpha}
%	{\basis;\sorting\comp x:S \judge P \hasType \typing\comp k:\alpha}
%	$		
%	&
%	$
%	\infer[$\ \rulelabel{cat}$]{\basis;\sorting \judge \receiveAct{k}{x}{P} \hasType \typing\comp k:\catType{\alpha}.\beta}
%	{\basis;\sorting \judge P \hasType \typing\comp k:\beta \comp x:\alpha}
%	$		
%	\\[.4cm]	
%	\multicolumn{2}{c}{	
%	$
%	\infer[(1 \leq j \leq n)\  $\ \rulelabel{Sel}$]{\basis;\sorting \judge \selectAct{k}{l_j}{P} \hasType \typing
%	\comp k:\selType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}}
%	{\basis;\sorting  \judge P \hasType \typing \comp k:\alpha_j}
%	$}
%	\\[.4cm]	
%	\multicolumn{2}{c}{	
%	$
%	\infer[$\ \rulelabel{Br}$]{\basis;\sorting \judge \branchAct{k}{\branch{l_1}{P_1} \branchSep \ldots \branchSep \branch{l_n}{P_n}} \hasType \typing
%	\comp k:\branchType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}}
%	{\basis;\sorting  \judge P_1 \hasType \typing \comp k:\alpha_1 && \ldots 
%	 && \basis;\sorting  \judge P_n \hasType \typing \comp k:\alpha_n}
%	$}
%	\\[.4cm]		
%	\multicolumn{2}{c}{
%	$
%	\infer[$\ \rulelabel{If}$]{\basis;\sorting \judge \ifthenelseAct{e}{P}{Q} \hasType \typing}
%	{\sorting  \judge e \hasType \boolType &&
%	\basis;\sorting  \judge P \hasType \typing && \basis;\sorting  \judge Q \hasType \typing}
%	$}	
%	\\[.4cm]	
%	$
%	\infer[$\ \rulelabel{Conc}$]{\basis;\sorting \judge P\mid Q \hasType \typing\comp \typing'}
%	{\basis;\sorting  \judge P \hasType \typing && \basis;\sorting  \judge Q \hasType \typing'}
%	$		
%	&
%	$
%	\infer[$\ \rulelabel{Res1}$]{\basis;\sorting \judge \res{a}{P} \hasType \typing}
%	{\basis;\sorting\comp a:S  \judge P \hasType \typing}
%	$	
%	\\[.4cm]	
%	$
%	\infer[$\ \rulelabel{Res2}$]{\basis;\sorting \judge \res{s}{P} \hasType \typing}
%	{\basis;\sorting  \judge P \hasType \typing\comp s:\alpha \comp \ce{s}:\dual{\alpha}}
%	$	
%	&
%	$
%	\infer[$\ \rulelabel{Res3}$]{\basis;\sorting \judge \res{s}{P} \hasType \typing}
%	{\basis;\sorting  \judge P \hasType \typing && s\ \text{ not in }\ \typing}
%	$	
%	\\[.4cm]	
%	$\basis\comp X:\typing;\sorting \judge X \hasType \typing$\ \ \rulelabel{Var}			
%	&
%	\infer[$\ \rulelabel{Rec}$]{\basis;\sorting \judge \recAct{X}{P} \hasType \typing}
%	{\basis\comp X:\typing;\sorting  \judge P \hasType \typing}	
%	\\[.2cm]
%	\hline
%	\end{tabular}
	\begin{tabular}{@{}c@{}}
	$\sorting \judge \ctrue \hasType \boolType$\ \ \rulelabel{Bool$_{\mathit{tt}}$}
	\qquad
	$\sorting \judge \cfalse \hasType \boolType$\ \ \rulelabel{Bool$_{\mathit{ff}}$}
	\qquad
	$\sorting \judge 1 \hasType \intType$\ \ \rulelabel{Int}	
	\ \ \ldots
	\\[.4cm]
	$
	\infer[$\ \rulelabel{Sum}$]{\sorting \judge +(e_1,e_2) \hasType \intType}
	{\sorting \judge e_1 \hasType \intType & & \sorting \judge e_2 \hasType \intType}
	$		
	\qquad
	$
	\infer[$\ \rulelabel{And}$]{\sorting \judge \wedge(e_1,e_2) \hasType \boolType}
	{\sorting \judge e_1 \hasType \boolType & & \sorting \judge e_2 \hasType \boolType}
	$		
	\ \ \ldots
	\\[.2cm]
	\end{tabular}
	\begin{tabular}{@{\hspace*{-1cm}}r@{\hspace*{.2cm}}r@{}}
	$\sorting\comp u:S \judge u \hasType S$\ \ \rulelabel{Id}	
	&
	\infer[$\ \rulelabel{Inact}$]{\basis;\sorting \judge \inact \hasType \typing}
	{\typing\ \  \text{completed}}	
	\\[.4cm]	
	$
	\infer[$\ \rulelabel{Req}$]{\basis;\sorting \judge \requestAct{u}{x}{P} \hasType \typing}
	{\sorting \judge u \hasType \sharedChanType{\alpha} & & \basis;\sorting \judge P \hasType \typing\comp x:\dual{\alpha}}
	$		
	&
	$
	\infer[$\ \rulelabel{Acc}$]{\basis;\sorting \judge \acceptAct{u}{x}{P} \hasType \typing}
	{\sorting \judge u \hasType \sharedChanType{\alpha} & & \basis;\sorting \judge P \hasType \typing\comp x:\alpha}
	$		
	\\[.4cm]	
	$
	\infer[$\ \rulelabel{Send}$]{\basis;\sorting \judge \sendAct{k}{e}{P} \hasType \typing\comp k:\outType{S}.\alpha}
	{\sorting \judge e \hasType S & & \basis;\sorting \judge P \hasType \typing\comp k:\alpha}
	$		
	&
	$
	\infer[$\ \rulelabel{Thr}$]{\basis;\sorting \judge \sendAct{k}{k'}{P} \hasType \typing\comp k:\thrType{\alpha}.\beta\comp k':\alpha}
	{\basis;\sorting \judge P \hasType \typing\comp k:\beta}
	$		
	\\[.4cm]	
	$
	\infer[$\ \rulelabel{Rcv}$]{\basis;\sorting \judge \receiveAct{k}{x}{P} \hasType \typing\comp k:\inpType{S}.\alpha}
	{\basis;\sorting\comp x:S \judge P \hasType \typing\comp k:\alpha}
	$		
	&
	$
	\infer[$\ \rulelabel{cat}$]{\basis;\sorting \judge \receiveAct{k}{x}{P} \hasType \typing\comp k:\catType{\alpha}.\beta}
	{\basis;\sorting \judge P \hasType \typing\comp k:\beta \comp x:\alpha}
	$		
	\\[.4cm]	
	\multicolumn{2}{c}{	
	$
	\infer[(1 \leq j \leq n)\  $\ \rulelabel{Sel}$]{\basis;\sorting \judge \selectAct{k}{l_j}{P} \hasType \typing
	\comp k:\selType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}}
	{\basis;\sorting  \judge P \hasType \typing \comp k:\alpha_j}
	$}
	\\[.4cm]	
	\multicolumn{2}{c}{	
	$
	\infer[$\ \rulelabel{Br}$]{\basis;\sorting \judge \branchAct{k}{\branch{l_1}{P_1} \branchSep \ldots \branchSep \branch{l_n}{P_n}} \hasType \typing
	\comp k:\branchType{\branch{l_1}{\alpha_1}, \ldots, \branch{l_n}{\alpha_n}}}
	{\basis;\sorting  \judge P_1 \hasType \typing \comp k:\alpha_1 && \ldots 
	 && \basis;\sorting  \judge P_n \hasType \typing \comp k:\alpha_n}
	$}
	\\[.4cm]		
	\multicolumn{2}{c}{
	$
	\infer[$\ \rulelabel{If}$]{\basis;\sorting \judge \ifthenelseAct{e}{P}{Q} \hasType \typing}
	{\sorting  \judge e \hasType \boolType &&
	\basis;\sorting  \judge P \hasType \typing && \basis;\sorting  \judge Q \hasType \typing}
	$}	
	\\[.4cm]	
	$
	\infer[$\ \rulelabel{Conc}$]{\basis;\sorting \judge P\mid Q \hasType \typing\comp \typing'}
	{\basis;\sorting  \judge P \hasType \typing && \basis;\sorting  \judge Q \hasType \typing'}
	$		
	&
	$
	\infer[$\ \rulelabel{Res1}$]{\basis;\sorting \judge \res{a}{P} \hasType \typing}
	{\basis;\sorting\comp a:S  \judge P \hasType \typing}
	$	
	\\[.4cm]	
	$
	\infer[$\ \rulelabel{Res2}$]{\basis;\sorting \judge \res{s}{P} \hasType \typing}
	{\basis;\sorting  \judge P \hasType \typing\comp s:\alpha \comp \ce{s}:\dual{\alpha}}
	$	
	&
	$
	\infer[$\ \rulelabel{Res3}$]{\basis;\sorting \judge \res{s}{P} \hasType \typing}
	{\basis;\sorting  \judge P \hasType \typing && s\ \text{ not in }\ \typing}
	$	
	\\[.4cm]	
	$\basis\comp X:\typing;\sorting \judge X \hasType \typing$\ \ \rulelabel{Var}			
	&
	\infer[$\ \rulelabel{Rec}$]{\basis;\sorting \judge \recAct{X}{P} \hasType \typing}
	{\basis\comp X:\typing;\sorting  \judge P \hasType \typing}	
	\\[.2cm]
	\hline
	\end{tabular}
	\vspace*{-.4cm}
	\caption{Typing system for \pic}
	\label{fig:typingSysytem}
	%\vspace*{-.4cm}
	\end{centering}
\end{figure}


%\subsection{Towards typing \respi}\label{sec:typeSystemRespi}
%
%%The first question that must be answered before
%A question that should be answered before defining a type discipline for a reversible 
%calculus is ``\emph{Should we type check the processes stored in the memories?}''.
%%
%The question arises from the fact that we should be able to determine if any \respi\ process is well-typed or not. 
%%
%Indeed, if the answer would be ``\emph{No}'', it would be sufficient to convert a 
%process of the reversible calculus into the corresponding process of the host calculus, by applying 
%the forgetful map $\forgetMap$ (Definition~\ref{def:forgetfulMap}), and then to type check 
%the latter process by using the typing system defined for the host calculus
%(Section~\ref{sec:typingPic}). 
%%
%However, in our case the answer to the question is ``\emph{Yes}'', because otherwise 
%typability would not be preserved under reduction (i.e.,  Subject Reduction Theorem would not 
%be satisfied).
%We show this with a simple example. 
%
%Let us consider the following \respi\ process (we omit trailing occurrences of~$\inact$):
%$$
%\begin{array}{l}
%M \ = \ \res{s,t_1',t_2',t_2''}\\
%\hspace{1.1cm}
%(\,
%\ptag{t_1'}\receive{\ce{s}}{x} \mid
%\ptag{t_2''}\send{s}{1+1} \mid
%\amem{\actEv{t_1}{\comAct{s}{1}{y}{P_1}{P_2}}{t_2}}{t_1'}{t_2'} 
%\\
%\hspace{1.3cm}
%\mid
%\cmem{t_2'}{\ifEv{(1>0)}{(\send{s}{1+1})}{(\send{s}{\cfalse})}}{t_2''}
%\,)
%\end{array}
%$$
%where 
%$P_1 = \receive{\ce{s}}{x}$ and 
%$P_2 = \ifthenelseAct{(y>0)}{\send{s}{y+1}}{\send{s}{\cfalse}}$.
%Intuitively, in previous reductions the thread $t_1$ sent 
%the value $1$ to the thread $t_2$;  then the received value has been 
%used by the continuation of $t_2$, i.e. the thread $t_2'$, to evaluate
%a conditional choice. 
%
%The corresponding \pic\ process is 
%$$
%\begin{array}{l}
%\forget{M} \ = \ \res{s}
%(\,
%\receive{\ce{s}}{x} \mid
%\send{s}{1+1} 
%\,)
%\end{array}
%$$
%which can be typed as follows:
%$$
%\scriptsize
%\infer[$\ \rulelabel{Res2}$]{\emptyset;\emptyset \judge 
%\res{s}{(\receiveAct{\ce{s}}{x}{\inact} \mid \sendAct{s}{1+1}{\inact})}
%\hasType \emptyset}
%{
%\infer[$\ \rulelabel{Conc}$]{\emptyset;\emptyset \judge 
%\receiveAct{\ce{s}}{x}{\inact} \mid
%\sendAct{s}{1+1}{\inact} \hasType  \ce{s}:\inpType{\intType}.\inactType \comp s:\outType{\intType}.\inactType}
%{
%\infer[$\ \rulelabel{Rcv}$]{\emptyset;\emptyset 
%\judge \receiveAct{\ce{s}}{x}{\inact}  \hasType \ce{s}:\inpType{\intType}.\inactType}
%{
%\infer[$\ \rulelabel{Inact}$]{\emptyset;x:\intType \judge \inact \hasType \ce{s}:\inactType}{}
%}
%&& 
%\infer[$\ \rulelabel{Send}$]{\emptyset;\emptyset \judge \sendAct{s}{1+1}{\inact} \hasType s:\outType{\intType}.\inactType}
%{
%	\infer[$\ \rulelabel{Sum}$]{\emptyset \judge 1+1 \hasType \intType}
%	{\infer[$\ \rulelabel{Int}$]{\emptyset \judge 1 \hasType \intType}{} }
%&& 
%\infer[$\ \rulelabel{Inact}$]{\emptyset;\emptyset \judge \inact \hasType s:\inactType}{}}
%}
%}
%$$
%
%However, process $M$ can perform a backward reduction and evolve to:
%$$
%\begin{array}{@{}l@{}}
%M' \, = \, \res{s,t_1',t_2'}
%(\,
%\ptag{t_1'}\receive{\ce{s}}{x} \mid
%\ptag{t_2'}\ifthenelseAct{(1>0)}{\send{s}{1+1}}{\send{s}{\cfalse}}
%\\\
%\hspace*{2.7cm}
%\mid
%\amem{\actEv{t_1}{\comAct{s}{1}{y}{P_1}{P_2}}{t_2}}{t_1'}{t_2'} 
%\,)
%\end{array}
%$$
%Unfortunately, $\forget{M'}$ is not typable, because the then-branch of the conditional choice
%is only typable under typing $s:\outType{\intType}.\inactType$ while 
%the else-branch under typing $s:\outType{\boolType}.\inactType$ and, therefore,
%the conditional choice is not typable (see rule \rulelabel{If} in Figure~\ref{fig:typingSysytem}).
%
%%Now, since memories must be typed, one could wonder if it is possible to type 
%%\respi\ processes in a na\"ive compositional way, i.e. by type checking terms different 
%%from memories by resorting to the typing system in Figure~\ref{fig:typingSysytem}
%%(thus, ignoring tags) and by type checking each memory by checking (again with the
%%same type system) the term producing the corresponding reduction. 
%%Unfortunately, in general this approach does not work. 
%%Consider indeed the following \respi\ process:
%Now, since memories must be typed, one could wonder if it is possible to na\"ively type 
%\respi\ processes by separately type checking the term
%resulting from the application of $\forgetMap$ and each single memory, 
%by using the typing system in \cite{YoshidaV07}. In particular, for each memory 
%we would check the stored term that generated the memory. 
%Unfortunately, in general this approach does not work. 
%Consider indeed the following \respi\ process:
%$$
%\begin{array}{@{}l@{}}
%N_1 \, = \\ \res{s,t_2',t_4'}
%(\,
%\ptag{t_1}\send{\ce{s}}{1} \mid
%\ptag{t_2'}\inact \mid
%\ptag{t_3}\receive{s}{x} \mid
%\ptag{t_4'}\inact
%\mid
%\amem{\actEv{t_2}{\comAct{s}{2}{y}{\inact}{\inact}}{t_4}}{t_2'}{t_4'} 
%\,)
%\end{array}
%$$
%By ignoring memory and tags, the rest of the term is typable 
%under typing $\emptyset$, because 
%$\send{\ce{s}}{1} \mid \receive{s}{x}$ is typable under typing
%$\typing \,=\, \ce{s}:\outType{\intType}.\inactType \comp s:\inpType{\intType}.\inactType$.
%Similarly, the term that performed the reduction generating the memory,
%i.e. $\send{\ce{s}}{2} \mid \receive{s}{y}$, is typable under the same typing $\typing$.
%%
%However, $N_1$ can perform the following backward reduction:
%$$
%N_1  \bwred \res{s}
%(\,
%\ptag{t_1}\send{\ce{s}}{1} \mid
%\ptag{t_2}\send{\ce{s}}{2} \mid
%\ptag{t_3}\receive{s}{x} \mid
%\ptag{t_4}\receive{s}{y} 
%\,) \ = \ N_2
%$$
%and $\forget{N_2}$ is not typable, because the typings of its parallel processes  
%are not composable (e.g., both $\send{\ce{s}}{1} \mid \receive{s}{x}$
%and $\send{\ce{s}}{2} \mid \receive{s}{y}$ have typing $\typing$,
%but $\typing \comp \typing$ is not defined). 
%
%From this we learn that, in general, a memory cannot be type checked in isolation
%without taking into account its context. 
%%
%Such context can be considered by extending the typing system 
%in \cite{YoshidaV07} with rules that permits typing memories and ignoring tag annotations 
%and tag restrictions.  In this way, typings of memories 
%and of threads are composed by means of the rule for parallel composition. 
%Thus, e.g., the process $N_1$ above is not typable by such typing system. 
%%
%However, type checking memories is a tricky task due to the dependencies 
%among them and with the threads outside memories. 
%If such dependencies would be ignored, unwanted conflicts during type checking could 
%arise. Thus, an appropriate type discipline requires to type check only the memories
%that do not depend on other memories; for each of them, we have to type check  
%the stored process without considering as context threads and memories that depend 
%on the memory under consideration. 
%
%In this work, we consider a simplified setting that permits avoiding to deal with 
%such dependencies. 
%%
%Specifically, we consider the class of \respi\ processes that are \emph{reachable from empty simple sessions},
%i.e., by extending Definition~\ref{def:reachable}, 
%processes obtained by means of forward and backward reductions from 
%processes with unique tags, no memory, no session initialised, no conditional choices and 
%recursions at top-level, and no delegation. 
%The characteristic of such processes is the following one: for each memory  
%inside a process, there exists within the process an ancestor memory 
%corresponding to the initialisation of the considered session. Thus, we
%can only focus on this latter kind of memories, which significantly simplifies 
%the theory. 
%
%Therefore, we slightly extend the typing system for \pic\ by adding the rules in Figure~\ref{fig:typingSysytem_respi}
%to those in Figure~\ref{fig:typingSysytem} (rules extending 
%\rulelabel{Res1}, \rulelabel{Res2} and \rulelabel{Res3} to \respi\ processes
%in a natural way have been omitted).
%We comment on salient points.
%%
%Process $\nil$ (rule \rulelabel{Nil}) is dealt with in a different way wit respect to $\inact$,
%because the former does not indicate the end of a thread but 
%just an empty process (which cannot be nested in action prefixes). 
%Therefore, $\nil$ is typable under any environment and typing.
%Rule \rulelabel{Thrd} permits ignoring tags and, hence, resorting to the typing system of 
%\pic\ for checking threads. Similarly, rule \rulelabel{Res'} permits ignoring 
%tag restrictions. 
%Rule \rulelabel{Conc'} simply extends \rulelabel{Conc} to \respi\ processes.
%Rule \rulelabel{Mem1} permits ignoring all memories but for those derived from 
%a session initialisation, which are checked by means of rule \rulelabel{Mem2}
%by resorting to the typing system of \pic\ for checking the generating term.  
%Notably, some sessions could be checked more than once (in case the corresponding initialisation 
%actions are nested in other processes), anyway this does not cause any conflict. 
%Notice also, that stored processes are closed, since they have given rise to a reduction. 
%%
%Results about such typing system and definition of a type discipline for a broader 
%class of \respi\ process are currently under investigation. 
%
%\begin{figure}[t]
%\footnotesize
%	\begin{centering}
%	\begin{tabular}{@{}r@{\qquad }r@{}}
%	$\basis;\sorting \judge \nil \hasType \typing$\ \ \rulelabel{Nil}
%	&
%	$
%	\infer[$\ \rulelabel{Conc'}$]{\basis;\sorting \judge M\mid N \hasType \typing\comp \typing'}
%	{\basis;\sorting  \judge M \hasType \typing && \basis;\sorting  \judge N \hasType \typing'}
%	$		
%	\\[.4cm]	
%	$
%	\infer[$\ \rulelabel{Res'}$]{\basis;\sorting \judge \res{t}{M} \hasType \typing}
%	{\basis;\sorting  \judge M \hasType \typing}
%	$	
%	&
%	\infer[$\ \rulelabel{Mem1}$]{\basis;\sorting \judge m \hasType \typing}
%	{m \neq \amem{\actEv{t_1}{\initAct{a}{x}{y}{P}{Q}{s}}{t_2}}{t_1'}{t_2'}}		
%	\\[.4cm]		
%	\infer[$\ \rulelabel{Thrd}$]{\basis;\sorting \judge (\ptag{t}P) \hasType \typing}
%	{\basis;\sorting \judge P \hasType \typing}	
%	&
%	\infer[$\ \rulelabel{Mem2}$]{\basis;\sorting \judge \amem{\actEv{t_1}{\initAct{a}{x}{y}{P}{Q}{s}}{t_2}}{t_1'}{t_2'} \hasType \typing}
%	{\basis;\sorting \judge (\ptag{t_1}\requestAct{a}{x}{P}\ \mid \ \ptag{t_2}\acceptAct{a}{y}{Q}) \hasType \typing}	
%	\\[.2cm]
%	\hline
%	\end{tabular}
%	\vspace*{-.4cm}
%	\caption{Typing System for \respi\ (additional rules)}
%	\label{fig:typingSysytem_respi}
%	\end{centering}
%\end{figure}


\subsubsection{Results}
We report here the main results concerning the type discipline, namely Subject Reduction and Type Safety, 
borrowed from \cite{YoshidaV07}. The former result states that well-typedness is preserved along 
computations, while the latter states that no interaction errors occur on well-typed processes.

\begin{theorem}[Subject Reduction]\label{th:subRedPiCalc}
If \mbox{$\basis;\sorting \judge P \hasType \typing$} with $\typing$ balanced and 
$P \red^* Q$, then \mbox{$\basis;\sorting \judge Q \hasType \typing'$} and $\typing'$ balanced.
\end{theorem}
\begin{proof}See proof of Theorem~3.3 in \cite{YoshidaV07}. 
\end{proof}

The notion of error, necessary to formalise Type Safety, is also taken from \cite{YoshidaV07}.
A \emph{k-process} is a process prefixed by subject $k$, while a \mbox{\emph{k-redex}} is the parallel composition 
of two $k$-processes either of the form 
$(\sendAct{\ce{k}}{e}{P_1} \ \mid \ \receiveAct{k}{x}{P_2})$, or
$(\selectAct{\ce{k}}{l_i}{P} \ \mid \ \branchAct{k}{\branch{l_1}{P_1}  \branchSep \ldots \branchSep \branch{l_n}{P_n}})$.
Then, $P$ is an \emph{error} if $P \congr \res{c}(Q \mid R)$ where $Q$ is, for some $k$, the parallel composition 
of either two $k$-processes that do not form a $k$-redex, or of three or more $k$-processes.

\begin{theorem}[Type Safety]\label{th:typeSafetyCalc}
A program typable under a balanced channel environment never reduces to an error.
\end{theorem}
\begin{proof}See proof of Theorem~3.4 in \cite{YoshidaV07}. 
\end{proof}


\subsection{Typing \respi}\label{sec:typeSystemRespi}
We show here how the notion of types, the typing system and the related results given  
for the \pic\ (Section~\ref{sec:typingPic}) can be reused for typing \respi. The key point 
is that we only consider reachable \respi\ processes originated from \respi\ programs 
that are well-typed (according to the typing discipline of \pic). In fact, by statically type 
checking \respi\ programs, we already check all possible interactions that they will perform. 
More specifically, Subject Reduction and Type Safety ensure that all runtime processes 
obtained from a program by means of (forward) reductions are interaction safe. Thus, since backward computations 
cannot lead to new runtime processes, but just go back to terms reachable from the program 
via forward reductions, there is no need of type checking the content of the memories 
in runtime processes.

Now, before formally showing how the typing discipline of \pic\ extends to \respi, we introduce 
a few auxiliary definitions and results. 

\begin{definition}[Reachable processes for typed \respi]\label{def:reachableResPi}
The set of \emph{reachable processes} for the typed \respi\ only contains: 
\emph{(i)} programs $M$ such that \mbox{$\basis;\sorting \judge \forget{M} \hasType \typing$}
with $\typing$ balanced, and (ii) runtime processes $M$ obtained by forward reductions 
from the above programs. 
\end{definition}

\begin{property}\label{prop:reachablePreserving}
Let $M$ be a reachable process. If $M \fwbwred M'$ then $M'$ is a reachable process.
\end{property}
\begin{proof}
The proof follows from Definition~\ref{def:reachableResPi} and Lemma~\ref{lemma:loop} (see~\ref{app:types}).  
\end{proof}

The notion of well-typedness for \respi, expressed by the judgement 
\mbox{$\basis;\sorting \judgeR M \hasType \typing$}, is defined in terms of the 
well-typedness notion introduced for the \pic.
\begin{definition}[Well-typedness]\label{def:wellTypedness}
\mbox{$\basis;\sorting \judgeR M \hasType \typing$}
if and only if 
$\basis;\sorting \judge \forget{M} \hasType \typing$,
with $\typing$ balanced.
\end{definition}

Thus, Subject Reduction extends to \respi\ terms as follows. 
\begin{theorem}[Subject Reduction]\label{th:subRedResPi}
Let $M$ be a reachable process. If \mbox{$\basis;\sorting \judgeR M \hasType \typing$} with $\typing$ balanced and 
$M \fwbwred M'$, then \mbox{$\basis;\sorting \judgeR M' \hasType \typing'$} and $\typing'$ balanced.
\end{theorem}
\begin{proof}The proof relies on Theorem~\ref{th:subRedPiCalc} (see~\ref{app:types}).  
\end{proof}

We conclude by showing how the notion of error and Type Safety of \pic\ extends to \respi. A \respi\ process 
$M$ is an \emph{error} if and only if $\forget{M}$ is an error. 

\begin{theorem}[Type Safety]\label{th:typeSafetyRespi}
A \respi\ program typable under a balanced channel environment never reduces to an error.
\end{theorem}
\begin{proof}
By the notion of \respi\ error and by Theorem~\ref{th:typeSafetyCalc}, we have that typable programs 
are not errors. Then, by Theorem~\ref{th:subRedResPi} we have the thesis. 
\end{proof}





%\subsection{Multiple providers scenario: typing}
\subsection{Typing the multiple providers scenario}
Coming back to the scenario introduced in Section~\ref{sec:example}
and specified in \respi\ in Section~\ref{sec:examplerespi}, 
we can easily verify that the process
$$
\ptag{t_1}P_{client}  \ \mid \ \ptag{t_2}P_{provider1}  \ \mid \ \ptag{t_3}P_{provider2}  
$$
is well-typed (assuming that the unspecified processes $P_{acc}$, $P_{neg}$, $Q_{acc}$ and $Q_{neg}$
are properly typable). 
In particular, the channel $a_{login}$ can be typed by the shared channel type
$$
\sharedChanType{\,\inpType{\mathsf{Request}}.\,\outType{\mathsf{Quote}}.\,
\branchType{\branch{l_{acc}}{\alpha_{acc}}\,,\,\branch{l_{neg}}{\alpha_{neg}}\,,\,\branch{l_{rej}}{\inactType}}\,}
$$
where we use sorts $\mathsf{Request}$ and $\mathsf{Quote}$ to type requests and quotes,
respectively.

Let us consider now a scenario where the client wishes to concurrently submit two different 
requests to the same provider, which would be able to concurrently serve them. 
Consider in particular the following specification of the client (the provider one is dual):
$$
\begin{array}{l}
\requestPrefix{a_{login}}{x}.\,(\,\send{x}{\mathsf{srv\_req\_1}}.\,P_{1} \ \mid \ \send{x}{\mathsf{srv\_req\_2}}.\,P_{2} \,)
\end{array}
$$
The new specification of the scenario is clearly not well-typed, due to the use of 
parallel threads within the same session. This %permits avoiding 
forbids us from mixing up messages related to different 
requests and wrongly delivering them. In order to properly concurrently submit separate requests,
the client must instantiate separate sessions with the provider, one for each request.

The session type discipline, indeed, forces concurrent interactions to follow structured patterns 
that guarantee the correctness of communication. 
%
For what concerns reversibility, linear use of session channels limits the 
effect of causal consistency, since concurrent interactions along the same session 
are prevented and, hence, the backtracking of a given session follows a single path. 
Of course, interactions along different sessions are still concurrent and, therefore, 
it is important to use a causal-consistent rollback to revert them.


