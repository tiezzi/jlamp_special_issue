% !TEX root = ../resPi_JLAMP.tex
\section{Related work}
\label{sect:relatedWork}
Our proposal combines the notion of (causal-consistent) reversibility with (typed) primitives supporting session-based interactions in concurrent systems. We review here some of the closely related works concerning either reversibility or session types.

%In recent years, reversible computing came to prominence as a suitable paradigm for designing and developing reliable 
%concurrent systems. Along this line of research, the works in the reversible computing field most closely related to ours are those concerning the definition of \emph{reversible process calculi}.

Forms of reversible computation can be found in different formalisms in the literature. For example, 
backward reductions are considered in the $\lambda$-calculus to define equality on 
expressions \cite{barendregt1985lambda}. Similar notions are used in the definitions of back and 
forth bisimulations \cite{NicolaMV90} 
on Labelled Transition Systems, and of reversible steps in Petri nets \cite{PN}.
More in practice, reversibility can be used for exploring different possibilities in a computation.
 For example, the Prolog language uses its backtracking capabilities to explore the state-space of a
derivation to find a solution for a given goal. 
However, in our paper we mainly focus on the use of reversible computing as a suitable paradigm for 
designing and developing reliable concurrent systems, which came to prominence in recent years. 
Along this line of research, the works in the reversible computing field most closely related to ours 
are those concerning the definition of \emph{reversible process calculi}. 
We briefly discuss the most relevant of them below, and refer the interested reader 
to \cite{BEATCS} for a comprehensive survey and a larger perspective.

Reversible CCS (RCCS)~\cite{DanosK04} is the first proposal of reversible calculus, from which all subsequent 
works drew inspiration. The host calculus (i.e., the non-reversible calculus extended 
with capabilities for reversibility) is CCS without recursive definitions and relabelling. 
%
To each currently running thread is associated an individual memory stack keeping track 
of past actions, as well as forks and synchronisations. Information pushed on the memory stacks,
upon doing a forward transition, can be then used for a rollback.
%
The memories also serve as a naming scheme and yield unique identifiers for threads.
%
When a process divides in two sub-threads, each sub-thread inherits the father memory together 
with a fork number (either $\langle 1 \rangle$ or $\langle 2 \rangle$) indicating which of the two 
sons the thread is. Then, in the case of a forward synchronisation, the synchronised threads exchange
their names (i.e., memories) in order to allows the corresponding backward synchronisation to 
take place. A drawback of this approach for memorising fork actions is that the parallel operator 
does not satisfy usual structural congruence rules as commutativity, associativity and 
nil process as neutral element. It is proved that RCCS is causally consistent, i.e.~the calculus 
allows backtrack along any causally equivalent past, where concurrent actions can be swapped and 
successive inverse actions can be cancelled. RCCS has been used for studying a general notion
of transactions in \cite{DanosK05}.

CCS-R~\cite{DanosK07a} is another reversible variant of CCS,
which however mainly aims at formalising biological systems.  
%
Like the previous calculus, it relies on memory stacks for storing information needed for 
backtracking, which now also record events corresponding to the unfolding of process 
definitions. Instead, differently from RCCS, specific identifiers are used to label threads;
in case of unfolding, sub-threads are labelled on-the-fly. Forking now does not 
exploit fork numbers, but it requires the memory stack of a given thread be empty before enabling 
the execution of its sons; this forces the sub-threads to share the memory that their father
had before the forking. As in RCCS, in case of synchronisation, the communicating threads 
exchange their identifiers. The transition system of CCS-R is proved to be reversible and it 
is demonstrated that CCS-R is sound and complete w.r.t.~CCS.

CCS with communication Keys (CCSK)~\cite{PhillipsU07} is a reversible process calculus
obtained by applying a general procedure to produce reversible calculi.
%
A relevant aspect of this approach is that it does not rely on memories for supporting backtracking. 
The idea is to maintain the structure of processes fixed throughout computations, thus
avoiding to consume guards and alternative choices, which is the source of irreversibility.
Past behaviour and discarded alternatives are then recorded in the syntax of terms.
This is realised by transforming the dynamic rules of the SOS semantics into static-like rules.
In this way, backward rules are obtained simply as symmetric versions of the forward ones.
%
To ensure that synchronisations are properly reverted, two communicating threads have to 
agree on a communication key, which will uniquely identify that communication. In this
way, the synchronising actions are locked together and can only be undone together. 
%
As usual, results showing that the method yields well-behaved transition relations are provided.
%
The proposed converting procedure can be applied to other calculi without name passing, 
such as ACP~\cite{BergstraK84} or CSP~\cite{BrookesHR84},
but it is not suitable for calculi with name binders, as \pic, which we are interested in this work.

$\rho\pi$~\cite{LaneseMS10} is a reversible variant of the higher-order \pic~\cite{HOpi}.
It borrows from RCCS the use of memories for keeping track of past actions. 
However, in $\rho\pi$ memories are not stacks syntactically associated to threads,
but they simply are terms, each one dedicated to a single communication, in parallel with 
processes. The connection between memories and threads is kept by resorting to 
identifiers in a way similar to CCSK. Fork handling relies on specific structured tags 
connecting the identifier of the father thread with the identifiers of its sub-threads. 
%
Besides proving that $\rho\pi$ is causally consistent, it is also shown that it can be
faithfully encoded into higher-order \pic.
%
Notably, differently from the approaches mentioned before,
the semantics of $\rho\pi$ is given in a reduction style. 
%
A variant of this calculus, called $\mathsf{roll}$-$\pi$~\cite{LaneseMSS11}, has been defined 
to control backward computations by means of a rollback primitive.  
%
The approaches proposed in \cite{LaneseMS10,LaneseMSS11} have been 
applied in \cite{GLMT} for reversing a variant of Klaim~\cite{NicolaFP98}.

Another reversible variant of \pic\ is R$\pi$~\cite{CristescuKV13}. 
Differently from $\rho\pi$, R$\pi$ considers a standard \pic\ (without choice and 
replication) as host calculus, and its semantics is defined in terms of a labelled transition relation. 
This latter point requires to introduce some technicalities to properly deal with scope extrusion. 
Similarly to RCCS,  this calculus relies on memory stacks, now recording events (i.e., consumed 
prefixes and related substitutions) and forking (by means of the fork symbol $\langle \uparrow \rangle$).
%
Results about the notion of causality induced by the semantics of the calculus are provided.

Reversible structures~\cite{CardelliL11} is a simple computational calculus, based on 
DSD~\cite{CardelliL09}, for modelling chemical systems. Since such systems are naturally 
reversible but have no backtracking memory, 
differently from most of the above proposals, reversible structures does not exploit memories. Instead, 
reversible structures maintain the structure of terms and use a special symbol $\,\hat{}\,$ to indicate the next operations 
(one forward and one backward) that a term can perform. Terms of the calculus are parallel compositions 
of signals and gates (i.e., terms that accept input signals and emit output signals), which interact 
according to a CCS-style model. When a forward synchronisation takes place, the executed gate input
is labelled by the identifier of the consumed signal, and the pointer symbol inside the gate is moved 
forward. The backwards computation is realised by executing these operations in a reverse way, thus 
releasing the output signal. As usual, the interplay between causal dependency and reversible structures
is studied, with the novelty that in this setting signal identifiers are not necessary unique.  

In our work we mainly take inspiration from the $\rho\pi$ approach. 
Indeed, all other approaches based on CCS and DSD cannot be 
directly applied to a calculus with name-passing. Moreover, the $\rho\pi$ approach is 
preferable to the R$\pi$ one because the former proposes a reduction semantics, which we are 
interested in, while the latter proposes a labelled semantics, which would complicate our theoretical
framework (in order to properly deal with scope extension of names). Specifically, 
we use unique (non-structured) tags for identifying threads and memories for recording 
taking place of actions, choices and forking. Each memory is devoted to storing the information
needed to revert a single event, and memories are connected each other, in order to keep track 
of computation history, by using tags as links.

For what concerns the related works on session-based calculi, it is worth noticing that we
consider a setting as standard and simple as possible, which is the one with 
synchronous binary sessions. In particular, our host calculus is the well-established variant of \pic\ 
introduced in \cite{YoshidaV07}, whose notation has been revised according to \cite{MostrousY09}.
We leave for future investigation the application of our approach to formalisms relying on the
other forms of sessions introduced in the literature, among which we would like to mention
asynchronous binary sessions~\cite{CoppoDY07},
multiparty asynchronous sessions~\cite{HondaYC08},
multiparty synchronous sessions~\cite{BejleriY09},
sessions with higher-order communication~\cite{MostrousY09},
sessions specifically devised for service-oriented computing~\cite{BorealeBNL08,BruniLMT08,CairesV10}.

Finally, the paper with the aim closest to ours is \cite{BarbaneraDd14}, where a formalism combining 
the notions of reversibility and session is proposed. This calculus is simpler than \respi, because it is an 
extension of the formalism of \emph{session behaviours} \cite{SB} without delegation (i.e., it is a sub-language of CCS) with 
a checkpoint-based backtracking mechanism. In fact, neither message nor channel passing are
considered in the host calculus. Concerning reversibility, only the behaviour prefixed by the lastly traversed 
checkpoint is recorded by a given party, that is each behaviour is simply paired with a one-size memory. 
Moreover, causal-consistency is not considered, because in this formalism parties just reduce in sequential way. 
Also commitable sessions are not taken into account. On the other hand, this formalism enabled the study of 
an extension of the compliance notion to the reversible setting.  